package com.sotanna.war;

import com.sotanna.core.Core;
import com.sotanna.core.Manager;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.damage.event.DeathEvent;
import com.sotanna.core.event.base.EListener;
import com.sotanna.core.event.bus.events.EventPriority;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.event.custom.PlayerMoveBlockEvent;
import com.sotanna.core.event.custom.TimingEvent;
import com.sotanna.core.player.Rank;
import com.sotanna.core.player.base.Tab;
import com.sotanna.core.util.streams.HighestBlock;
import com.sotanna.core.util.uU.VectorU;
import com.sotanna.core.world.base.EWorld;
import com.sotanna.war.build.BuildStore;
import com.sotanna.war.build.base.StoredBuild;
import com.sotanna.war.command.BuildCmd;
import com.sotanna.war.command.BuildSaveCmd;
import com.sotanna.war.command.worldfix.WorldFixCmd;
import com.sotanna.war.game.WarGame;
import com.sotanna.war.game.build.ArcherWarrior;
import com.sotanna.war.game.build.CavalierWarrior;
import com.sotanna.war.game.build.SavantWarrior;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.types.WarKitPvP;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.bukkit.event.EventPriority.HIGHEST;

public class Warriors extends Manager<Warriors> {

    public static Boolean DoSave = true;
    public static WarGame<?> CurrentGame;

    public Map<String, WarriorBuild<?>> WarriorBuilds = new HashMap<>();

    private Tab JOIN_TAB;
    private EWorld GameWorld;

    private final Map<Player, List<Block>> FakeSent = new HashMap<>();
    private final BuildStore BuildStore;

    public Warriors(Plugin plugin) {
        super(plugin, "Warriors");
        BuildStore = new BuildStore(thisClass());
    }

    @Override
    public void onEnable() {
        GameWorld = Core.Worlds.load("World_Sorrowdale");
        GameWorld.Spawnpoint(GameWorld.location(0.5, 64, 0.5, -90, 0));

        WarriorBuilds.put("Savant"   , new SavantWarrior());
        WarriorBuilds.put("Archer"   , new ArcherWarrior());
        WarriorBuilds.put("Cavalier" , new CavalierWarrior());

        CurrentGame = new WarKitPvP().enable();
        JOIN_TAB = new Tab("         &8&l&m--=<=[&r &c&lWarriors &8&l&m]=>=--&r         \n ", "\n \n&c&lWarriors &b&l"+CurrentGame.type().name());

        onlineOnJoin();

        WarriorBuilds.values().forEach(EListener::enable);
        Core.Commands.add(new BuildCmd(), new BuildSaveCmd(), new WorldFixCmd());

        BuildStore.enable();
    }

    @Override
    public void onDisable() {
        BuildStore.disable();

        WarriorBuilds.values().forEach(EListener::disable);
        if (DoSave) onlineOnQuit();

        WarriorBuilds.clear();
    }

    @Override
    public void onJoin(Player player) {
        CurrentGame.join(player);
        JOIN_TAB.send(player);
        player.teleport(GameWorld.spawnpoint());
    }

    @Override
    public void onQuit(Player player) {
        List<Block> fakes = FakeSent.remove(player);
        if (fakes != null) fakes.forEach(block -> sendChange(player, block, false, false));

        CurrentGame.quit(player);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if (inSpawn(e.getPlayer()) && !Rank.bypassing(e.getPlayer())) e.setCancelled(true);
    }

    @EventHandler(priority = HIGHEST)
    public void onArmorTouch(InventoryClickEvent e) {
        if (e.getSlotType() == InventoryType.SlotType.ARMOR || e.getSlotType() == InventoryType.SlotType.QUICKBAR) {
            if (!Rank.bypassing(((Player) e.getWhoClicked()))) e.setCancelled(true);
        }
    }

    @EventHandler(priority = HIGHEST)
    public void onDrop(PlayerDropItemEvent e) {
        if (!Rank.bypassing(e.getPlayer())) e.setCancelled(true);
    }

    @EventWatcher
    public void onDie(DeathEvent e) {
        e.DropItems(false);
    }

    @EventWatcher(priority = EventPriority.First)
    public void spawnDamage(DamageEvent e) {
        if (!e.isPlayer()) return;
        if (inSpawn(e.damagedPlayer()) || (e.byPlayer() && inSpawn(e.damagerPlayer()))) e.Cancelled(true);
    }

    @EventWatcher
    public void onLeave(PlayerMoveBlockEvent e) {

        if (e.player().getGameMode() == GameMode.SURVIVAL || e.player().getGameMode() == GameMode.ADVENTURE) {
            double distance = VectorU.distance(GameWorld.spawnpoint(), e.to().getLocation().add(0.5, 0, 0.5));

            if (distance > 9.6 && distance < 20) {
                Block work = HighestBlock.at(e.to()), next;
                for (int x = -6; x < 7; x++) {
                    for (int z = -6; z < 7; z++) {
                        next = work.getRelative(x, 0, z);

                        if (next.getType() == Material.WOOL && next.getData() == ((byte) 14))
                            for (int i = 1; i < 6; i++) sendChange(e.player(), next.getRelative(BlockFace.UP, i), true, true);
                    }
                }
            }
        }

        if (FakeSent.containsKey(e.player())) {
            List<Block> blocks = FakeSent.get(e.player());

            for (Iterator<Block> iterator = blocks.iterator(); iterator.hasNext(); ) {
                Block block = iterator.next();
                double dist = VectorU.distance(block, e.to());

                if (dist > 5 || e.player().getGameMode() == GameMode.CREATIVE || e.player().getGameMode() == GameMode.SPECTATOR) {
                    iterator.remove();
                    sendChange(e.player(), block, false, false);
                }
            }
            if (blocks.isEmpty()) FakeSent.remove(e.player());
        }
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent e) {
        if (e.toWeatherState()) e.setCancelled(true);
    }

    @EventWatcher
    public void onWall(TimingEvent e) {
        if (!e.tick()) return;
        for (Player player : FakeSent.keySet()) {
            List<Block> blocks = FakeSent.get(player);
            blocks.forEach(block -> sendChange(player, block, true, false));
        }
    }

    public WarriorBuild<?> byName(String name) {
        return WarriorBuilds.get(name);
    }

    public StoredBuild build(Player player) {
        return BuildStore.build(player);
    }

    public boolean inSpawn(Player player) {
        return VectorU.distance(GameWorld.spawnpoint(), player.getLocation().getBlock().getLocation().add(0.5, 0.5, 0.5)) < 9.5;
    }

    private void sendChange(Player player, Block block, boolean show, boolean add) {
        player.sendBlockChange(block.getLocation(), show ?  Material.STAINED_GLASS : Material.AIR, show ? ((byte) 14) : ((byte) 0));
        if (show && add) {
            if (!FakeSent.containsKey(player)) FakeSent.put(player, new ArrayList<>());
            FakeSent.get(player).add(block);
        }
    }
}
