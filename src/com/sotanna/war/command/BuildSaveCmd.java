package com.sotanna.war.command;

import com.sotanna.core.command.base.Command;
import com.sotanna.core.player.Rank;
import com.sotanna.core.util.F;
import com.sotanna.war.Warriors;
import org.bukkit.entity.Player;

import java.util.List;

public class BuildSaveCmd extends Command {

    public BuildSaveCmd() {
        super("BuildSave", Rank.Owner, "bs");
    }

    @Override
    public void execute(Player player, Rank rank, String used, List<String> args) {
        Warriors.DoSave = !Warriors.DoSave;
        message(F.enDis(Warriors.DoSave));
    }

}
