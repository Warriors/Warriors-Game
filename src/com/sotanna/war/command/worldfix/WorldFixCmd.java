package com.sotanna.war.command.worldfix;

import com.sotanna.core.command.base.Cmd;
import com.sotanna.core.command.base.Command;
import com.sotanna.core.player.Rank;
import org.bukkit.entity.Player;

import java.util.List;

@Cmd(name = "WorldFix", rank = Rank.Owner)
public class WorldFixCmd extends Command {

    @Override
    public void execute(Player player, Rank rank, String used, List<String> args) {
        new WorldFixer(player.getWorld()).run();
    }
}
