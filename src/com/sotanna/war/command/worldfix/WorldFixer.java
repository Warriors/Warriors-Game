package com.sotanna.war.command.worldfix;

import com.sotanna.core.util.task.Call;
import com.sotanna.core.util.task.Run;
import com.sotanna.core.util.uU.RandomU;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.ArrayList;
import java.util.List;

public class WorldFixer {

    private static final Call<Block>
            FOR_GRASS = data -> data.getRelative(BlockFace.DOWN).setType(Material.DIRT),
            FOR_STAINED = data -> {
                int beach = 0;
                for (int i = -2; i < 3; i++) for (int j = -2; j < 3; j++) if (data.getRelative(i, 0, j).getType() == Material.GRASS) beach++;
                data.getRelative(BlockFace.DOWN).setType(RandomU.choice(beach * 10) ? Material.SAND : RandomU.choice(85) ? Material.LAPIS_BLOCK : RandomU.choice(70) ? Material.PACKED_ICE : Material.ICE);
            },
            FOR_WATER = data -> data.setTypeIdAndData(Material.STAINED_GLASS.getId(), ((byte) 3), true),
            FOR_STONE = data -> {
                Material type = RandomU.choice(80) ? Material.STONE : Material.COBBLESTONE;
                data.setTypeIdAndData(type.getId(), ((byte) (type == Material.STONE ? RandomU.choice(70) ? 0 : 5 : 0)), true);
            };

    private World world;

    public WorldFixer(World world) {
        this.world = world;
    }

    public World world() {
        return world;
    }

    public void run() {
        List<Point> points = new ArrayList<>();
        for (int x = -500; x < 501; x++) for (int z = -500; z < 501; z++) points.add(new Point(x, z));
        final int[] next = {0};

        Run.submit(() -> {
            for (int i = 0; i < 2000; i++) {
                if (points.size() <= next[0]) break;
                Point point = points.get(next[0]++);
                Block on;
                for (int y = 199; y >= 0; y--) {
                    on = world().getBlockAt(point.x, y, point.z);
                    Call<Block> work = doFor(on);
                    if (work != null) work.call(on);
                }
                System.out.println("On " + next[0] + " / " + points.size() + "["+point.x+", "+point.z+"]");
            }
        }).Until(() -> next[0] == points.size()).goRepeat(1);
    }

    private Call<Block> doFor(Block block) {
        switch (block.getType()) {
            case GRASS: return FOR_GRASS;
            case WATER:
            case STATIONARY_WATER:
                return FOR_WATER;
            case STAINED_GLASS:
                if (block.getData() == ((byte) 3)) return FOR_STAINED;
            case STONE: return FOR_STONE;
            default:
                return null;
        }
    }

    private static class Point {
        public int x, z;

        public Point(int x, int z) {
            this.x = x;
            this.z = z;
        }
    }
}
