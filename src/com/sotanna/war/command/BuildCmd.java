package com.sotanna.war.command;

import com.sotanna.core.command.base.Cmd;
import com.sotanna.core.command.base.Command;
import com.sotanna.core.player.Rank;
import com.sotanna.core.util.C;
import com.sotanna.war.War;
import com.sotanna.war.gui.BuildGui;
import org.bukkit.entity.Player;

import java.util.List;

@Cmd(name = "Build", aliases = {"b"})
public class BuildCmd extends Command {

    @Override
    public void execute(Player player, Rank rank, String used, List<String> args) {
        if (!War.Warriors.inSpawn(player) && !Rank.bypassing(player)) message(C.Error + "You can't do this here").send(player);
        else new BuildGui(player, War.Warriors.build(player)).open(player);
    }
}
