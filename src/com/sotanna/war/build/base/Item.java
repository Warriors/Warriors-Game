package com.sotanna.war.build.base;

import com.sotanna.core.util.item.EItem;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;

import java.util.HashMap;
import java.util.Map;

public class Item {

    private Integer slot, worth;

    private final String material;
    private final Integer data, amount;

    private final Map<String, Integer> Enchants = new HashMap<>();

    public Item(Integer slot, Material material, Integer data, Integer amount, Integer worth) {
        this.slot = slot;
        this.material = material == null ? null : material.name();
        this.data = data;
        this.amount = amount;
        this.worth = worth;
    }

    public Integer slot() {
        return slot;
    }

    public Item Slot(Integer slot) {
        this.slot = slot;
        return this;
    }

    public Material material() {
        return material == null ? null : Material.valueOf(material);
    }

    public Integer data() {
        return data;
    }

    public Integer amount() {
        return amount;
    }

    public Integer worth() {
        return worth;
    }

    public Map<String, Integer> Enchants() {
        return Enchants;
    }

    public Item Enchant(Enchantment enchantment, int level) {
        Enchants.put(enchantment.getName(), level);
        return this;
    }

    public EItem get() {
        EItem eItem = new EItem(material(), data()).Amount(amount == null || amount == 0 ? 1 : amount).Unbreakable(true).Flag(ItemFlag.HIDE_UNBREAKABLE);
        Enchants().forEach((name, level) -> eItem.Enchant(Enchantment.getByName(name), level));

        return eItem;
    }

    public static Item change(Item item, int slot) {
        return new Item(slot, item.material(), item.data(), item.amount(), item.worth());
    }

    @Override
    public String toString() {
        return "Item{" +
                "slot=" + slot +
                ", worth=" + worth +
                ", material='" + material + '\'' +
                ", data=" + data +
                ", amount=" + amount +
                ", Enchants=" + Enchants +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return new EqualsBuilder()
                .append(worth, item.worth)
                .append(material, item.material)
                .append(data, item.data)
                .append(Enchants, item.Enchants)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(worth)
                .append(material)
                .append(data)
                .append(Enchants)
                .toHashCode();
    }
}
