package com.sotanna.war.build.base;

import com.sotanna.core.util.F;
import com.sotanna.core.util.item.Armor;
import com.sotanna.core.util.item.EItem;
import com.sotanna.core.util.item.Weapon;
import com.sotanna.core.util.type.Builder;
import com.sotanna.core.util.type.Gsonable;
import com.sotanna.core.util.type.Logging;
import com.sotanna.war.War;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.PassiveSkill;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Build implements Gsonable<Build>, Builder<Build>, Logging {

    public static final transient int MaxSkillTokens = 12, MaxPassiveTokens = 30, MaxItemTokens = 20;


    //region Base Build Attributes
    private final String name;

    private final Map<String, Integer> Skills = new HashMap<>();
    private final List<Item> Items = new ArrayList<>(), Armors = new ArrayList<>();
    private int skillTokens = MaxSkillTokens, passiveTokens = MaxPassiveTokens, itemTokens = MaxItemTokens;
    //endregion

    //region Runtime Attributes
    private transient LinkedHashMap<WarriorSkill<?, ?>, Integer> SkillMap = new LinkedHashMap<>();
    private transient LinkedHashMap<Integer, Item> ItemMap = new LinkedHashMap<>();
    private transient TreeMap<Armor.Position, Armor.Type> ArmorMap = new TreeMap<>((o1, o2) -> o2.compareTo(o1));
    private transient boolean locked = false, enabled = false;
    //endregion

    public Build(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }

    //region Tokens
    public int skillTokens() {
        return skillTokens;
    }

    public Build SkillTokens(int skillTokens) {
        this.skillTokens = skillTokens;
        return this;
    }

    public int passiveTokens() {
        return passiveTokens;
    }

    public Build PassiveTokens(int passiveTokens) {
        this.passiveTokens = passiveTokens;
        return this;
    }

    public int itemTokens() {
        return itemTokens;
    }

    public Build ItemTokens(int itemTokens) {
        this.itemTokens = itemTokens;
        return this;
    }

    public Build refreshBal() {
        skillTokens   = MaxSkillTokens;
        passiveTokens = MaxPassiveTokens;
        itemTokens    = MaxItemTokens;

        SkillMap().forEach((skill, level) -> {
            int worth = skill.initWorth() + (level - 1) * skill.worth();

            if (skill instanceof PassiveSkill) passiveTokens -= worth;
            else skillTokens -= worth;
        });

        ArmorMap().forEach(((position, type) -> passiveTokens -= position.worth(type)));
        ItemMap().forEach((slot, item) -> itemTokens -= item.worth());

        return this;
    }

    public <W extends WarriorSkill<?, ?>> boolean afford(W skill, boolean level) {
        int balance = skill instanceof PassiveSkill ? passiveTokens() : skillTokens();
        return balance >= (level ? skill.worth() : skill.initWorth());
    }

    public boolean afford(Armor.Position position, Armor.Type type) {
        int fromLast = position.worth(type.last());
        return passiveTokens() + fromLast >= position.worth(type);
    }

    public boolean afford(Item item) {
        return itemTokens() >= item.worth();
    }
    //endregion

    //region Runtime Settings
    public LinkedHashMap<WarriorSkill<?, ?>, Integer> SkillMap() {
        return SkillMap;
    }

    public LinkedHashMap<Integer, Item> ItemMap() {
        return ItemMap;
    }

    public TreeMap<Armor.Position, Armor.Type> ArmorMap() {
        return ArmorMap;
    }

    public boolean locked() {
        return locked;
    }

    public Build Lock(boolean locked) {
        this.locked = locked;
        return this;
    }
    //endregion

    //region Skills
    public Integer level(String name) {
        return Skills.get(name);
    }

    public <W extends WarriorSkill<?, ?>> Integer level(W skill) {
        return Skills.get(skill.name());
    }

    public List<WarriorSkill<?, ?>> skills(SkillType... types) {
        return F.collect(F.filter(SkillMap().keySet(), skill -> F.stream(types).anyMatch(type -> type == skill.type())));
    }

    public <W extends WarriorSkill<?, ?>> Build Skill(W skill, int... level) {
        int lvl = level.length > 0 ? level[0] : 1;

        Skills.compute(skill.name(), (name, sLvl) -> SkillMap.compute(skill, (sk, lv) -> lvl == -1 ? null : lvl));
        return this;
    }
    //endregion

    public Build Armor(Armor.Position position, Armor.Type type) {
        Armors.removeIf(item -> Armor.position(item.material()) == position);
        Armors.add(new Item(-1, position.type(type), 0, 1, 0));

        ArmorMap().compute(position, (pos, typ) -> type);
        return this;
    }

    public Build Item(Item item) {
        Weapon.Position position = Weapon.position(item.material());
        if (position != null) {
            Items.removeIf(other -> {
                Weapon.Position otherPos = Weapon.position(other.material());
                boolean remove = otherPos != null && otherPos == position;
                if (remove) {
                    ItemMap().remove(other.slot());
                    item.Slot(other.slot());
                }
                return remove;
            });
        }

        if (item.material() != null) Items.add(item);
        ItemMap().compute(item.slot(), (slot, ite) -> item.material() == null ? null : item);

        if (ItemMap().isEmpty()) Items.clear();

        return this;
    }

    public ItemStack[] armors() {
        ItemStack[] armors = new ItemStack[4];
        ArmorMap().forEach(((position, type) -> armors[position.ordinal()] = new EItem(position.type(type)).Unbreakable(true).Flag(ItemFlag.HIDE_UNBREAKABLE).get()));
        return armors;
    }

    public ItemStack[] items() {
        ItemStack[] items = new ItemStack[9];
        ItemMap().forEach((slot, item) -> items[slot] = item.get().get());
        for (int i = 0; i < items.length; i++) if (items[i] == null) items[i] = new ItemStack(Material.AIR);

        return items;
    }

    public void enable(Player player) {
        WarriorBuild<?> warriorBuild = warrior();
        if (warriorBuild == null) return;

        warriorBuild.Build(player, thisClass());

        player.getInventory().clear();
        player.getInventory().setArmorContents(armors());
        ItemStack[] items = items();
        for (int i = 0; i < items.length; i++) player.getInventory().setItem(i, items[i]);
        player.updateInventory();

        enabled = true;
    }

    public void disable(Player player) {
        WarriorBuild<?> warriorBuild = warrior();
        if (warriorBuild == null || !warriorBuild.using(player)) return;

        warriorBuild.Build(player, null);

        player.getInventory().clear();
        player.getInventory().setArmorContents(new ItemStack[4]);
        player.updateInventory();

        enabled = false;
    }

    public boolean using(Player player) {
        return enabled;
    }

    public WarriorBuild<?> warrior() {
        return War.Warriors.byName(name());
    }

    @Override
    public Build onFrom() {
        locked   = enabled = false;
        SkillMap = new LinkedHashMap<>();
        ItemMap  = new LinkedHashMap<>();
        ArmorMap = new TreeMap<>((o1, o2) -> o2.compareTo(o1));

        WarriorBuild<?> build = War.Warriors.byName(name());
        if (build != null) {
            Skills.forEach((name, level) -> {
                WarriorSkill<?, ?> skill = build.byName(name);
                if (skill != null) SkillMap.put(skill, level);
            });
        }

        Items.forEach(item -> ItemMap.put(item.slot(), item));
        Armors.forEach(item -> ArmorMap.put(Armor.position(item.material()), Armor.type(item.material())));
        return thisClass();
    }

    @Override
    public Class<? extends Build> type() {
        return Build.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Build build = (Build) o;

        return new EqualsBuilder()
                .append(skillTokens, build.skillTokens)
                .append(passiveTokens, build.passiveTokens)
                .append(itemTokens, build.itemTokens)
                .append(name, build.name)
                .append(Skills, build.Skills)
                .append(Items, build.Items)
                .append(Armors, build.Armors)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(Skills)
                .append(Items)
                .append(Armors)
                .append(skillTokens)
                .append(passiveTokens)
                .append(itemTokens)
                .toHashCode();
    }
}
