package com.sotanna.war.build.base;

import com.sotanna.core.Core;
import com.sotanna.core.util.F;

import java.util.ArrayList;
import java.util.List;

public class StoredBuild {

    private final List<Build> builds = new ArrayList<>();

    public List<Build> builds() {
        return builds;
    }

    public StoredBuild Builds(String json) {
        Build[] builds = Core.Gson().fromJson(json, Build[].class);
        if (builds != null) builds().addAll(F.list(builds));
        builds().forEach(Build::onFrom);

        return this;
    }

    public StoredBuild Build(Build build) {
        builds().removeIf(old -> F.equals(old.name(), build.name()));
        builds().add(build);

        return this;
    }

    public Build build(String name) {
        return F.filter(builds(), build -> F.equals(build.name(), name)).findFirst().orElse(null);
    }

    public boolean locked() {
        return F.filter(builds(), Build::locked).findFirst().isPresent();
    }

    @Override
    public String toString() {
        return Core.Gson().toJson(builds());
    }
}
