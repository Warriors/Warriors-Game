package com.sotanna.war.build;

import com.sotanna.core.Manager;
import com.sotanna.core.database.call.MySqlCall;
import com.sotanna.core.util.F;
import com.sotanna.war.Warriors;
import com.sotanna.war.build.base.StoredBuild;

import org.bukkit.entity.Player;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class BuildStore extends Manager<BuildStore> {

    private static final String TableName = "WarBuilds", RowName = "Build";


    private final Warriors Warriors;
    private final Map<Player, StoredBuild> Builds = new HashMap<>();

    public BuildStore(Warriors warriors) {
        super(warriors.plugin(), "BuildStore");

        Warriors = warriors;
    }

    @Override
    public void onEnable() {
        new MySqlCall() {
            @Override
            public void callEx(Connection data) throws Exception {
                preparedStatement = data.prepareStatement("Create Table If Not Exists " + TableName + "(UUID CHAR(36) Primary Key Not Null, " + RowName + " LONGTEXT)");
                preparedStatement.execute();

                onlineOnJoin();
            }
        }.execute();
    }

    @Override
    public void onDisable() {
        onlineOnQuit();
    }

    @Override
    public void onJoin(Player player) {
        new MySqlCall() {
            @Override
            public void callEx(Connection data) throws Exception {
                preparedStatement = data.prepareStatement("Select " + RowName + " From " + TableName + " Where UUID=?");
                preparedStatement.setString(1, player.getUniqueId().toString());

                resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    Builds.put(player, new StoredBuild().Builds(resultSet.getString(RowName)));
                }
                else {
                    StoredBuild storedBuild = new StoredBuild();
                    Warriors.WarriorBuilds.values().forEach(warriorBuild -> storedBuild.Build(warriorBuild.defaultBuild()));
                    Builds.put(player, storedBuild);

                    save(player, false);
                }

                Warriors.WarriorBuilds.values().forEach(warrior -> {
                    boolean has = F.filter(build(player).builds(), build -> F.equals(warrior.name(), build.name())).findFirst().isPresent();
                    if (!has) {
                        build(player).Build(warrior.defaultBuild());
                    }
                });

                build(player).builds().get(0).enable(player);
            }
        }.execute();
    }

    @Override
    public void onQuit(Player player) {
        save(player, true);
    }

    public StoredBuild build(Player player) {
        return Builds.get(player);
    }

    private void save(Player player, boolean remove) {
        final String json = build(player).toString();

        new MySqlCall() {
            @Override
            public void callEx(Connection data) throws Exception {
                preparedStatement = data.prepareStatement("Insert Into " + TableName + " (UUID, "+RowName+") Values (?, ?) On Duplicate Key Update " + RowName +"=Values("+RowName+")");
                preparedStatement.setString(1, player.getUniqueId().toString());
                preparedStatement.setString(2, json);

                preparedStatement.execute();

                if (remove) Builds.remove(player);
            }
        }.execute();
    }
}
