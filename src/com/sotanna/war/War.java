package com.sotanna.war;

import com.sotanna.core.util.C;
import com.sotanna.core.util.type.EPlugin;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class War extends JavaPlugin implements EPlugin {

    public static Plugin Instance;

    public static Warriors Warriors;

    @Override
    public void onEnable() {
        Instance = this;

        Warriors = new Warriors(Instance);
        loadManagers(Instance);

        C.Register("Lore" , C.Purple + C.Bold);
        C.Register("Name" , C.White + C.Bold);
        C.Register("Good" , C.Green + C.Bold);
        C.Register("Bad"  , C.Red + C.Bold);
        C.Register("None" , C.Gray + C.Bold);
        C.Register("LSep" , C.Sep + C.Strike);
    }

    @Override
    public String name() {
        return "War";
    }
}
