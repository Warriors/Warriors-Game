package com.sotanna.war.types;

import com.sotanna.core.Core;
import com.sotanna.core.damage.event.DeathEvent;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.scoreboard.BoardBuilder;
import com.sotanna.core.util.F;
import com.sotanna.war.game.WarGame;
import com.sotanna.war.game.WarType;
import com.sotanna.war.json.ScoreStore;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class WarKitPvP extends WarGame<WarKitPvP> {

    private final Map<Player, Integer> Deaths = new HashMap<>();
    private final ScoreStore ScoreStore = new ScoreStore();

    public WarKitPvP() {
        super("KitPvP", WarType.KitPvP);
    }

    @Override
    public WarKitPvP enable() {
        super.enable();
        ScoreStore.enable();

        return thisClass();
    }

    @Override
    public WarKitPvP disable() {
        super.disable();
        ScoreStore.disable();

        return thisClass();
    }

    @Override
    public boolean looseStreak() {
        return false;
    }

    @Override
    public WarKitPvP join(Player player) {
        info("On Join " + player.getName());
        Deaths.put(player, 0);

        Core.Accounts.account(player, account -> {
            account.data("WarKitPvP-Kills", reponse -> ScoreMod(player, F.number(reponse.response()).intValue()));
            account.data("WarKitPvP-Deaths", reponse -> DeathMod(player, F.number(reponse.response()).intValue()));

            ScoreStore.loadOrCreate(player.getUniqueId()).Kills(score(player)).Deaths(deaths(player));
        });

        return super.join(player);
    }

    @Override
    public WarKitPvP quit(Player player) {
        Core.Accounts.account(player, account -> {
            int kills = score(player), deaths = deaths(player);
            log("Player" + player.getName() + " " + kills + ":" + deaths);
            account.Data("WarKitPvP-Kills", String.valueOf(kills));
            account.Data("WarKitPvP-Deaths", String.valueOf(deaths));

            ScoreStore.loadOrCreate(player.getUniqueId()).Kills(kills).Deaths(deaths);

            Deaths.remove(player);
            super.quit(player);
        });
        return thisClass();
    }

    @Override
    public BoardBuilder boardBuilder(Player player) {
        int kills = score(player), deaths = deaths(player);
        double kd = kills == 0 || deaths == 0 ? 0 : F.format(((double) kills) / ((double) deaths), 2);

        return new BoardBuilder().spacer()
                                 .next("&8&lOnline   ")
                                 .next("" + Core.Players.count())
                                 .spacer()
                                 .next("&a&lKills")
                                 .next("" + kills)
                                 .spacer()
                                 .next("&c&lDeaths")
                                 .next("" + deaths)
                                 .spacer()
                                 .next("&bK&7/&bD")
                                 .next("" + kd)
                                 .spacer()
                                 .next("&8&lPing")
                                 .next("&a" + Core.Players.ping(player) + "&fms");
    }

    public int deaths(Player player) {
        return Deaths.containsKey(player) ? Deaths.get(player) : 0;
    }

    public void DeathMod(Player player, int amount) {
        Deaths.put(player, Deaths.get(player) + amount);
    }

    @EventWatcher
    public void onDeath(DeathEvent e) {
        if (!e.isPvP() || e.cancelled()) return;
        DeathMod(e.damagedPlayer(), 1);
    }
}
