package com.sotanna.war.game.skills.types.archer.weapon;

import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.effect.SoundEffect;
import com.sotanna.core.event.bus.events.EventPriority;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.event.custom.TimingEvent;
import com.sotanna.core.util.uU.VectorU;
import com.sotanna.war.War;
import com.sotanna.war.game.build.ArcherWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ArcherPounce extends WarriorSkill<ArcherPounce, ArcherWarrior> {

    private final Map<Player, Integer> Charge = new HashMap<>();
    private final SoundEffect Spring = new SoundEffect(new ESound(Sound.BAT_TAKEOFF, 5, 0.4), new ESound(Sound.FIRE_IGNITE, 1, 0.6));

    public ArcherPounce() {
        super("Pounce", SkillType.Weapon, "Jump at your enemies", "startling them into a haze");
    }

    @Override
    public Long time() {
        return 8000L;
    }

    @Override
    public ArcherPounce disable() {
        Charge.clear();
        return super.disable();
    }

    @EventWatcher(priority = EventPriority.First)
    public void onFall(DamageEvent e) {
        if (e.isPlayer() && e.cause() == EntityDamageEvent.DamageCause.FALL && hasAtt(e.damagedPlayer())) {
            e.Cancelled(true);
            e.damaged().setFallDistance(0);
        }
    }

    @EventWatcher
    public void onCharge(TimingEvent e) {
        if (!e.tick()) return;

        warrior().users().forEach(player -> {
            int level = level(player);
            if (level > 0) {
                if (player.isBlocking() && !War.Warriors.inSpawn(player)) {
                    if (Charge.containsKey(player) || done(player)) {

                        if (!Charge.containsKey(player)) Charge.put(player, 0);

                        int on = Charge.put(player, Charge.get(player) + 1);
                        if (on == 10) pounce(player, level);
                        else new ESound(Sound.NOTE_BASS, ((float) (on * 0.1))).sNear(player.getLocation(), 25);
                    }
                } else if (Charge.containsKey(player)) pounce(player, level);
            }
        });
    }

    private void pounce(Player player, int level) {
        double strength = (Charge.get(player) * 0.1) + (level * 0.3);

        Vector direction = VectorU.direction(player);
        Spring.sNear(player.getLocation(), 30);
        player.setVelocity(direction.normalize().multiply(strength));

        useSkill(player, level);
        att(player, 5, TimeUnit.SECONDS);

        Charge.remove(player);
        use(player, time() - ((time() / 4) - (level * 500)));
    }
}
