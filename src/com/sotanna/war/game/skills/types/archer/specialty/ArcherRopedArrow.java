package com.sotanna.war.game.skills.types.archer.specialty;

import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.event.custom.ArrowHitBlockEvent;

import com.sotanna.core.util.uU.VectorU;
import com.sotanna.war.game.build.ArcherWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.PreparingBowSkill;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class ArcherRopedArrow extends PreparingBowSkill<ArcherRopedArrow, ArcherWarrior> {

    private static final Vector Multiply = new Vector(1.55, 2, 1.55);

    public ArcherRopedArrow() {
        super("Roped Arrow", SkillType.Specialty, "Send off your arrow with a rope attached", "and pull yourself in");
    }

    @Override
    public Long time() {
        return 3000L;
    }

    @Override
    public void onShoot(EntityShootBowEvent e, Player shooter, Arrow arrow, int level) {}

    @Override
    public boolean canPrepare(PlayerInteractEvent e) {
        return super.canPrepare(e) && e.getItem().getType() == Material.BOW && e.getPlayer().isSneaking();
    }

    @Override
    public void onHitBlock(ArrowHitBlockEvent e, Player shooter, int level) {
        pull(shooter, e.block().getLocation());
    }

    @Override
    public void onHitPlayer(DamageEvent e, Player shooter, int level) {
        pull(shooter, e.damaged().getEyeLocation());
    }

    private void pull(Player player, Location towards) {
        Vector vector = VectorU.trace(player.getLocation(), towards);
        if (vector.getY() >= -0.9 && vector.getY() <= 0) vector.setY(0.5);
        player.setVelocity(vector.multiply(Multiply));
    }
}
