package com.sotanna.war.game.skills.types.archer.ranged;

import com.sotanna.core.Core;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.effect.Particle;
import com.sotanna.core.effect.ParticleE;
import com.sotanna.core.effect.SoundEffect;
import com.sotanna.core.util.streams.EntityIs;
import com.sotanna.core.event.custom.ArrowHitBlockEvent;
import com.sotanna.core.util.F;
import com.sotanna.core.util.uU.RandomU;
import com.sotanna.core.util.uU.VectorU;
import com.sotanna.war.War;
import com.sotanna.war.game.build.ArcherWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.PreparingBowSkill;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.concurrent.TimeUnit;

public class ArcherFlame extends PreparingBowSkill<ArcherFlame, ArcherWarrior> {

    private final Particle Fire = new Particle(ParticleE.LAVA).Amount(6).Speed(0.04).OffSet(3.5);
    private final SoundEffect Boom = new SoundEffect(new ESound(Sound.EXPLODE, 10, 0.7), new ESound(Sound.FIRE, 5, 0.9));

    public ArcherFlame() {
        super("Flame", SkillType.Ranged, "Shoot flaming arrows that", "explode on impact");
    }

    @Override
    public Long time() {
        return 12000L;
    }

    @Override
    public boolean canPrepare(PlayerInteractEvent e) {
        return super.canPrepare(e) && e.getItem().getType() == Material.BOW;
    }

    @Override
    public void onShoot(EntityShootBowEvent e, Player shooter, Arrow arrow, int level) {
        arrow.setFireTicks(RandomU.gInt(60, (int) (60 + ((e.getForce() + 0.5) * (20 + level)))));
    }

    @Override
    public void onHitBlock(ArrowHitBlockEvent e, Player shooter, int level) {
        boom(shooter, e.arrow(), level, e.block().getLocation());
    }

    @Override
    public void onHitPlayer(DamageEvent e, Player shooter, int level) {
        boom(shooter, ((Arrow) e.projectile()), level, e.damaged().getLocation());
    }

    private void boom(Player shooter, Arrow arrow, int level, Location location) {
        Boom.sNear(arrow.getLocation(), 30);
        Fire.sNear(arrow.getLocation());

        F.filter(Core.Players.nearby(location, RandomU.gInt(5, 6 + (level / 2))), EntityIs.get(shooter).not()).forEach(player -> {
            if (!War.Warriors.inSpawn(player)) {
                player.setVelocity(VectorU.trace(location, player.getEyeLocation(), 1.7));
                player.setFireTicks(arrow.getFireTicks() + (20 * level));
                damage(player, shooter, arrow.getFireTicks() * 50, TimeUnit.MILLISECONDS);
            }
        });
    }
}
