package com.sotanna.war.game.skills.types.archer.passive;

import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.event.custom.TimingEvent;
import com.sotanna.core.util.C;
import com.sotanna.core.util.uU.TimeU;
import com.sotanna.war.game.build.ArcherWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.PassiveSkill;

import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class ArcherSharpShooter extends PassiveSkill<ArcherSharpShooter, ArcherWarrior> {

    private static final Long ResetTime = 6000L;
    private static final Map<Player, Stack> StackMap = new HashMap<>();

    private static final ESound Ding = new ESound(Sound.NOTE_PLING, 1.5f), Reset = new ESound(Sound.NOTE_PLING, 0.1f);

    public ArcherSharpShooter() {
        super("SharpShooter", "Every shot hit increases damage");
    }

    @Override
    public Long time() {
        return 0L;
    }

    @Override
    public int maxLevel() {
        return 4;
    }

    @Override
    public SkillType requires() {
        return SkillType.Ranged;
    }

    @EventWatcher
    public void onHitE(DamageEvent e) {
        if (!e.isPvP() || !e.byProjectile() || !(e.projectile() instanceof Arrow) || e.cancelled() || e.damaged().equals(e.damager())) return;

        int level = level(e.damagerPlayer());
        if (level == 0) return;

        Stack stack = StackMap.computeIfAbsent(e.damagerPlayer(), Stack::new);

        message(C.Msg + "Damage Increase " + C.Green + "+" + stack.level()).send(e.damagerPlayer());
        Ding.send(e.damagerPlayer());

        e.Damage(e.damage() + stack.level());
        damage(e.damagedPlayer(), e.damagerPlayer(), 1, TimeUnit.SECONDS);

        if (stack.level() < level) stack.Level();
        stack.Ready(false);
    }

    @EventWatcher
    public void onTry(TimingEvent e) {
        if (!e.second()) return;

        StackMap.entrySet().removeIf(entry -> {
            Stack stack = entry.getValue();
            if (TimeU.hasPast(stack.update() + ResetTime) && stack.ready()) {
                Reset.send(entry.getKey());
                message(C.Error + "Damage Increase reset").send(entry.getKey());
                return true;
            }
            stack.Ready(true);
            return false;
        });
    }


    private static class Stack {

        private int level = 1;
        private long update = 0L;
        private boolean ready = false;

        public Stack(Player uuid) {}

        public int level() {
            return level;
        }

        public Stack Level() {
            ++level;
            this.update = System.currentTimeMillis();
            return this;
        }

        public long update() {
            return update;
        }

        public boolean ready() {
            return ready;
        }

        public Stack Ready(boolean ready) {
            this.ready = ready;
            return this;
        }
    }
}
