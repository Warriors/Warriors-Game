package com.sotanna.war.game.skills.types.archer.ranged;

import com.sotanna.core.Core;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.effect.Particle;
import com.sotanna.core.effect.ParticleE;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.event.custom.ArrowHitBlockEvent;
import com.sotanna.core.event.custom.TimingEvent;
import com.sotanna.core.util.uU.PacketU;
import com.sotanna.war.War;
import com.sotanna.war.game.build.ArcherWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.PreparingBowSkill;
import com.sotanna.war.game.skills.base.SpecialtySkill;

import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityShootBowEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ArcherPiercingShot extends PreparingBowSkill<ArcherPiercingShot, ArcherWarrior> {

    private static final ESound Shoot = new ESound(Sound.ENDERMAN_TELEPORT, 6, 0.2);
    private static final Particle Explode = new Particle(ParticleE.EXPLOSION_NORMAL).Amount(2).OffSet(0.2);


    private final Map<Player, Double> Charge = new HashMap<>();

    public ArcherPiercingShot() {
        super("Piercing Shot", SkillType.Ranged, "Charge your bow and pierce", "the armor of your enemy");
    }

    @Override
    public Long time() {
        return 10000L;
    }

    @Override
    public boolean shouldShoot(EntityShootBowEvent e, Player shooter, Arrow arrow, int level) {
        return Charge.containsKey(shooter);
    }

    @Override
    public void onShoot(EntityShootBowEvent e, Player shooter, Arrow arrow, int level) {
        if (!Charge.containsKey(shooter)) return;

        Core.Entities.metadata(arrow, name() + "-Charge", Charge.remove(shooter));
        Shoot.sNear(shooter.getLocation(), 20);
    }

    @Override
    public void onHitBlock(ArrowHitBlockEvent e, Player shooter, int level) {
        Particle.playBlockBreak(e.block());
        Explode.sNear(e.block().getLocation());
    }

    @Override
    public void onHitPlayer(DamageEvent e, Player shooter, int level) {
        if (!e.isPvP() || !e.byProjectile() || !(e.projectile() instanceof Arrow)) return;

        Arrow arrow = ((Arrow) e.projectile());
        if (!arrow.hasMetadata(name())) return;

        double damage = Core.Entities.metadata(arrow, name()+ "-Charge").asDouble();

        e.Damage(e.damage() + (0.45 * damage)).UseArmor(false);
        damage(e.damagedPlayer(), e.damagerPlayer(), 2, TimeUnit.SECONDS);
    }

    @EventWatcher
    public void onTime(TimingEvent e) {
        if (!e.fastest()) return;

        warrior().users().forEach(player -> {
            if (!War.Warriors.inSpawn(player) && prepared(player)) {
                int level = level(player);
                if (level > 0) {
                    if (PacketU.chargingBow(player) && player.isSneaking()) {
                        Charge.compute(player, ((p, aDouble) -> aDouble == null ? 1.0 : aDouble == level ? aDouble : aDouble + 1));
                        new ESound(Sound.BAT_LOOP, 0.5, ((float) (0.2 * Charge.get(player)))).sNear(player.getLocation(), 10);
                    } else Charge.remove(player);
                }
            }
        });
    }

}
