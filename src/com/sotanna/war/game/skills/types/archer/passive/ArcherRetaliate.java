package com.sotanna.war.game.skills.types.archer.passive;

import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.effect.SoundEffect;
import com.sotanna.core.event.bus.events.EventPriority;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.event.custom.TimingEvent;
import com.sotanna.war.game.build.ArcherWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.PassiveSkill;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class ArcherRetaliate extends PassiveSkill<ArcherRetaliate, ArcherWarrior> {

    private static final Double HealthTrigger = 6.0;
    private final SoundEffect Smash = new SoundEffect(new ESound(Sound.WITHER_HURT, 0.9, 0.3), new ESound(Sound.HURT_FLESH, 1, 0.5));

    public ArcherRetaliate() {
        super("Retaliate", "Attack when you have low", "health to do extra damage");
    }

    @Override
    public Long time() {
        return 7000L;
    }

    @Override
    public boolean notifying() {
        return false;
    }

    @Override
    public SkillType requires() {
        return null;
    }

    @EventWatcher(priority = EventPriority.Last)
    public void onSkill(DamageEvent e) {
        if (e.cancelled()) return;

        if (e.isPvP()) {
            if (prepared(e.damagerPlayer())) {
                int level = level(e.damagerPlayer());

                e.Damage(e.damage() + (0.30 * e.damage()) + ((double) level)).UseArmor(false);
                Smash.sNear(e.damaged().getLocation(), 20);

                useSkill(e.damagerPlayer(), level);
                damage(e.damagedPlayer(), e.damagerPlayer(), 2, TimeUnit.SECONDS);
                cancelAtt(e.damagerPlayer());
                return;
            }
        }

        int level = level(e.damagedPlayer());
        if (level == 0) return;

        if ((e.damaged().getHealth() - e.damage()) <= HealthTrigger && !prepared(e.damagedPlayer())) {
            if (!hasAtt(e.damagedPlayer()) && use(e.damagedPlayer())) {
                att(e.damagedPlayer(), 1 + (level * 2), TimeUnit.SECONDS);
                prepareSkill(e.damagedPlayer());
            }
        }
    }

    @EventWatcher
    public void onWearOff(TimingEvent e) {
        if (!e.second()) return;

        for (Iterator<Player> iterator = prepared().iterator(); iterator.hasNext(); ) {
            Player player = iterator.next();

            if (!hasAtt(player)) {
                iterator.remove();
                wearOffSkill(player);
            }
        }
    }
}
