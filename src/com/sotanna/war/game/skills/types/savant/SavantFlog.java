package com.sotanna.war.game.skills.types.savant;

import com.sotanna.core.Core;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.util.streams.EntityIs;
import com.sotanna.core.util.F;
import com.sotanna.core.util.item.ItemFactory;
import com.sotanna.core.util.task.Run;
import com.sotanna.core.util.uU.ActionU;
import com.sotanna.core.util.uU.RandomU;
import com.sotanna.core.world.pos.Region;
import com.sotanna.war.War;
import com.sotanna.war.game.build.SavantWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class SavantFlog extends WarriorSkill<SavantFlog, SavantWarrior> {

    private final ESound Whoosh = new ESound(Sound.HORSE_BREATHE, 3, 0.13);

    public SavantFlog() {
        super("Flog", SkillType.Secondary, "Flog your enemy", "with your mighty hammer");
    }

    @Override
    public int maxLevel() {
        return 4;
    }

    @Override
    public Long time() {
        return 10000L;
    }

    @Override
    public void onInteract(PlayerInteractEvent e) {
        int level = level(e.getPlayer());
        if (level == 0 || !e.hasItem() || !ItemFactory.isAxe(e.getItem()) || !ActionU.Right.isType(e.getAction())) return;

        if (!use(e.getPlayer(), time() + (1200 * level))) return;
        useSkill(e.getPlayer(), level);
        prepareSkill(e.getPlayer(), true);

        Run.submit(() -> {
            final Location target = Core.Players.targetBlock(e.getPlayer(), ((int) (level * (2 * 0.75)))).getLocation(), location = e.getPlayer().getLocation();
            final List<Item> items = new ArrayList<>();

            HashSet<Material> materials = new HashSet<>();
            Region near = new Region(Core.Worlds.load(location.getWorld()), location.clone().add((-level) - 1, -2, (-level) - 1).toVector(), location.clone().add(level + 1, level, level + 1).toVector());
            materials.addAll(near.blocks().stream().map(Block::getType).collect(Collectors.toList()));

            for (int i = 0; i < (level + 1) / 2; i++) {
                Material choice = RandomU.element(materials);
                if (!ItemFactory.dropable(choice)) continue;
                Item item = Core.Entities.drop(e.getPlayer().getEyeLocation(), new ItemStack(choice));
                item.setCustomName(RandomU.gInt(99999999) + "");
                item.setPickupDelay(99999999);
                item.setFireTicks(e.getPlayer().getFireTicks());

                double mult = 0.25 + (0.15 * level);

                item.setVelocity(location.getDirection().add(new Vector((0.2 - (RandomU.gInt(50) / 100d)) * mult, (RandomU.gInt(30) / 100d) * mult, (0.2 - (RandomU.gInt(50) / 100d)) * mult)).multiply(2));
                items.add(item);
                item.setMetadata(e.getPlayer().getUniqueId().toString(), new FixedMetadataValue(Core.Instance, 1));
            }

            Whoosh.sNear(target, 15);
            track(items, e.getPlayer());
        }).Until(() -> !prepared(e.getPlayer()) || !ItemFactory.isAxe(e.getItem()))
           .Only(30 + (15 * level))
           .Done(() -> wearOffSkill(e.getPlayer(), true)).goRepeat(2);

        Run.submit(() -> {
            if (prepared(e.getPlayer())) wearOffSkill(e.getPlayer());
        }).goLaterSecs(3 + (level));
    }

    private void track(List<Item> items, Player player) {
        Run.submit(() -> {
            for (Iterator<Item> itemI = items.iterator(); itemI.hasNext(); ) {
                Item item = itemI.next();
                if (!item.isValid() || item.getTicksLived() > 60 || item.isOnGround()) {
                    item.remove();
                    itemI.remove();
                    continue;
                }

                F.filter(Core.Entities.nearby(item.getLocation(), 2), EntityIs.get(item, player).not()).filter(entity -> entity instanceof Player).forEach(entity -> {
                    if (!War.Warriors.inSpawn(((Player) entity))) {
                        entity.setVelocity(item.getVelocity().multiply(0.1).setY(0.2));
                        damage(((Player) entity), player, 3, TimeUnit.SECONDS);
                        if (item.getFireTicks() >= entity.getFireTicks()) entity.setFireTicks(item.getFireTicks());
                    }
                });
            }

        }).Until(items::isEmpty).Done(() -> {
            items.forEach(Entity::remove);
            items.clear();
        }).goRepeat(1);
    }

}
