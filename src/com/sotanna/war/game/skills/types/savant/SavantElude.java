package com.sotanna.war.game.skills.types.savant;

import com.sotanna.core.effect.ESound;
import com.sotanna.core.effect.Particle;
import com.sotanna.core.effect.ParticleE;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.event.custom.TimingEvent;
import com.sotanna.core.util.streams.HighestBlock;
import com.sotanna.core.util.uU.VectorU;
import com.sotanna.war.War;
import com.sotanna.war.game.build.SavantWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.util.Vector;

public class SavantElude extends WarriorSkill<SavantElude, SavantWarrior> {

    private final Particle Smoke = new Particle(ParticleE.CLOUD).Amount(15).Speed(0.01).OffSet(0.9);
    private final ESound Fizz = new ESound(Sound.FIZZ, 2, 0.6);

    public SavantElude() {
        super("Elude", SkillType.Weapon, "Run into a wall while blocking","to jump above it");
    }

    @Override
    public Long time() {
        return 6000L;
    }

    @EventWatcher
    public void onBlock(TimingEvent e) {
        if (!e.tick()) return;

        warrior().users().forEach(player -> {
            int level = level(player);
            if (level > 0) {
                if (player.isBlocking() && !War.Warriors.inSpawn(player)) {
                    Block front = player.getEyeLocation().add(VectorU.direction(player).setY(0.2).multiply(1.4)).getBlock();
                    if (front.getType().isSolid()) {
                        if (use(player)) {
                            useSkill(player, level);
                            Fizz.sNear(player.getLocation(), 15);
                            Smoke.sNear(player.getLocation());

                            player.teleport(HighestBlock.at(front).getLocation().add(0.5, 1, 0.5).setDirection(VectorU.direction(player)));
                            player.setVelocity(new Vector(0, 0, 0));
                            Smoke.sNear(player.getLocation());
                        }
                    }
                }
            }
        });
    }
}
