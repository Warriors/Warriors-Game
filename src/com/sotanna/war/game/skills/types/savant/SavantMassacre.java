package com.sotanna.war.game.skills.types.savant;

import com.sotanna.core.damage.Condition;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.util.streams.HighestLocation;
import com.sotanna.core.util.task.Run;
import com.sotanna.core.util.uU.RandomU;
import com.sotanna.core.util.uU.VectorU;
import com.sotanna.war.game.build.SavantWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.concurrent.TimeUnit;

public class SavantMassacre extends WarriorSkill<SavantMassacre, SavantWarrior> {

    private final ESound Growl = new ESound(Sound.BLAZE_DEATH, 5, 1.6);

    public SavantMassacre() {
        super("Massacre", SkillType.Weapon, "Block when your enemy attacks", "to jump behind them and strike");
    }

    @Override
    public Long time() {
        return 10000L;
    }

    @EventWatcher
    public void onBlock(DamageEvent e) {
        if (!e.isPvP() || e.cancelled()) return;

        int level = level(e.damagedPlayer());
        if (level == 0) return;

        Player player = e.damagedPlayer(), damager = e.damagerPlayer();
        if (!player.isBlocking() || !use(player)) return;
        e.Damage(0);

        Vector push = VectorU.trace(player, e.damager());
        player.setVelocity(push.multiply(1));

        useSkill(player, level);

        damage(damager, player, 5, TimeUnit.SECONDS);
        Condition.effect(damager, RandomU.gInt(60, 100), Condition.Blindness, Condition.Slow);
        damager.setPlayerTime(19000, false);

        Run.submit(() -> {
            Growl.sNear(player.getLocation(), 15);
            Vector facing = VectorU.direction(damager);
            Location behind = HighestLocation.at(damager.getLocation().add(VectorU.behind(facing)));
            behind.setDirection(VectorU.direction(damager));

            player.teleport(behind);
            Growl.sNear(damager.getLocation(), 15);
        }).goLater(7);

        Run.submit(damager::resetPlayerTime).goLaterSecs(((int) (time() / 1000)));
    }
}
