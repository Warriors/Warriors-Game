package com.sotanna.war.game.skills.types.savant;

import com.sotanna.core.damage.Condition;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.effect.Particle;
import com.sotanna.core.effect.ParticleE;
import com.sotanna.core.event.bus.events.EventPriority;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.player.Rank;
import com.sotanna.core.util.item.ItemFactory;
import com.sotanna.core.util.task.Run;
import com.sotanna.war.War;
import com.sotanna.war.game.build.SavantWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.PassiveSkill;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Team;

import java.util.concurrent.TimeUnit;

import static org.bukkit.event.EventPriority.MONITOR;

public class SavantGhost extends PassiveSkill<SavantGhost, SavantWarrior> {

    private final ESound Enable = new ESound(Sound.HORSE_BREATHE, 0.3f), Disable = new ESound(Sound.ENDERMAN_TELEPORT, 5, 1.5);
    private final Particle Smoke = new Particle(ParticleE.SMOKE_NORMAL).Amount(20).Speed(0.01).OffSet(0.5);

    public SavantGhost() {
        super("Ghost", "Enter the void to", "slip away unseen");
    }

    @Override
    public Long time() {
        return 11000L;
    }

    @Override
    public SkillType requires() {
        return SkillType.Secondary;
    }

    @Override
    public SavantGhost enable() {
        showBoards();
        return super.enable();
    }

    @EventWatcher(priority = EventPriority.Last)
    public void onDamage(DamageEvent e) {
        if (e.cancelled() || !e.isPlayer() || !hasAtt(e.damagedPlayer())) return;
        e.Damage(e.damage() * 0.10);
    }

    @EventHandler
    public void onVoid(PlayerDropItemEvent e) {
        if (!ItemFactory.isAxe(e.getItemDrop().getItemStack())) return;
        Player player = e.getPlayer();

        int level = level(player);
        if (level == 0) return;

        int time = ((int) ((time() - (4000 - (level * 1000))))) / 50;

        e.setCancelled(true);

        if (!War.Warriors.inSpawn(player) && use(e.getPlayer())) {
            player.getEquipment().setArmorContents(new ItemStack[4]);
            build(player).Lock(true);

            game().Boards().forEach((owner, board) -> board.OTeam(player, board.teamFor(owner).getName()).update());

            Condition.effect(player, Condition.Invisibility, time);
            att(player, time * 50, TimeUnit.MILLISECONDS);

            Smoke.sNear(player.getLocation());
            Enable.sNear(player.getLocation(), 20);
            useSkill(player, level);

            Run.submit(() -> {
                player.getEquipment().setArmorContents(build(player).Lock(false).armors());

                game().Boards().forEach((owner, board) -> board.OTeam(player, Rank.of(player).boardName()).update());

                Condition.clear(player, Condition.Invisibility);
                wearOffSkill(player);

                Disable.sNear(player.getLocation(), 20);
                Smoke.sNear(player.getLocation());
            }).goLater(time);
        }
    }

    @EventHandler(priority = MONITOR)
    public void onJoin(PlayerJoinEvent e) {
        showBoards();
    }

    private void showBoards() {
        game().Boards().forEach(((player, board) -> board.OTeams().values().forEach((team) -> {
            Team reg = board.team(team);
            if (reg != null) reg.setCanSeeFriendlyInvisibles(true);
        })));
    }
}
