package com.sotanna.war.game.skills.types.cavalier;

import com.sotanna.core.Core;
import com.sotanna.core.damage.Condition;
import com.sotanna.core.util.C;
import com.sotanna.core.util.F;
import com.sotanna.core.util.item.ItemFactory;
import com.sotanna.core.util.uU.ActionU;
import com.sotanna.core.util.uU.VectorU;
import com.sotanna.war.game.build.CavalierWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class CavalierHiltJab extends WarriorSkill<CavalierHiltJab, CavalierWarrior> {

    public CavalierHiltJab() {
        super("Hilt Jab", SkillType.Weapon, "Jab your opponent with the", "hilt of your sword");
    }

    @Override
    public Long time() {
        return 11000L;
    }

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if (e.isCancelled() || !ActionU.Right.isType(e.getAction()) || !ItemFactory.isSword(e.getItem())) return;

        int level = level(e.getPlayer());
        if (level == 0) return;

        if (use(e.getPlayer())) {
            Location front = e.getPlayer().getLocation().add(VectorU.direction(e.getPlayer()).multiply(1.5));

            List<Player> nearby = F.filter(Core.Players.nearby(front, 2), pl -> !pl.equals(e.getPlayer())).collect(Collectors.toList());
            if (nearby.size() == 0) message(C.Error + "You missed!").send(e.getPlayer());
            else {
                Player player = nearby.get(0);
                message(C.Msg + "You jabbed " + C.Value + player.getName()).send(e.getPlayer());

                damage(player, e.getPlayer(), 2, TimeUnit.SECONDS);
                Core.Damage.newDamage(e.getPlayer(), player, 1 + (1.75 * level));
                Condition.effect(player, level * 20, 4, Condition.Blindness, Condition.Slow);
            }
        }
    }
}
