package com.sotanna.war.game.skills.types.cavalier;

import com.sotanna.core.Core;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.util.streams.EntityIs;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.util.F;
import com.sotanna.war.game.build.CavalierWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

import org.bukkit.event.entity.EntityDamageEvent;

import java.util.concurrent.TimeUnit;

public class CavalierRive extends WarriorSkill<CavalierRive, CavalierWarrior> {

    public CavalierRive() {
        super("Rive", SkillType.Secondary, "Attack with such rage that you", "have no real target");
    }

    @Override
    public Long time() {
        return null;
    }

    @EventWatcher
    public void onHit(DamageEvent e) {
        if (!e.isPvP() || e.cause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK || e.cancelled() || e.tagged()) return;

        int level = level(e.damagerPlayer());
        if (level == 0) return;

        F.filter(Core.Players.nearby(e.damaged().getLocation(), ((int) (1 + (2 + (0.5 * level))))), EntityIs.get(e.damagerPlayer(), e.damagedPlayer()).not()).forEach(player -> {
            damage(player, e.damagerPlayer(), 500, TimeUnit.MILLISECONDS);
            Core.Damage.newDamage(e.damagerPlayer(), player, e.damage() * (0.25 * level), EntityDamageEvent.DamageCause.ENTITY_ATTACK, null, name(), true, true);
        });
    }
}
