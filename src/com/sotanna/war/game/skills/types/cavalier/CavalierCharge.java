package com.sotanna.war.game.skills.types.cavalier;

import com.sotanna.core.damage.Condition;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.damage.event.DeathEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.effect.SoundEffect;
import com.sotanna.core.event.bus.events.EventPriority;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.util.item.ItemFactory;
import com.sotanna.core.util.uU.ActionU;
import com.sotanna.war.game.build.CavalierWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

import org.bukkit.Sound;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.concurrent.TimeUnit;

public class CavalierCharge extends WarriorSkill<CavalierCharge, CavalierWarrior> {

    private final SoundEffect Scream = new SoundEffect(new ESound(Sound.ENDERMAN_SCREAM, 3, 0), new ESound(Sound.STEP_WOOD, 5, 0));
    private final ESound HitSound = new ESound(Sound.IRONGOLEM_HIT, 5, 0.9);

    public CavalierCharge() {
        super("Charge", SkillType.Secondary, "Run at your enemies with the speed", "of 4 raging bulls");
    }

    @Override
    public Long time() {
        return 10000L;
    }

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if (e.isCancelled() || !ItemFactory.isAxe(e.getItem()) || !ActionU.Right.isType(e.getAction())) return;

        int level = level(e.getPlayer());
        if (level == 0) return;

        if (use(e.getPlayer())) {
            useSkill(e.getPlayer(), level);
            att(e.getPlayer(), 9, TimeUnit.SECONDS);
            Condition.effect(e.getPlayer(), Condition.Speed, 80 + (level * 20), 3);

            Scream.sNear(e.getPlayer().getLocation(), 30);
        }
    }

    @EventWatcher(priority = EventPriority.Last)
    public void onHit(DamageEvent e) {
        if (!e.isPvP() || !hasAtt(e.damagerPlayer()) || e.cancelled()) return;

        int level = level(e.damagerPlayer());
        if (level == 0) return;

        Condition.effect(e.damagedPlayer(), Condition.Slow, 80, level, false, false, true);
        HitSound.sNear(e.damaged().getLocation(), 20);

        cancelAtt(e.damagerPlayer());
        Condition.clear(e.damagerPlayer(), Condition.Speed);
    }

    @EventWatcher
    public void onKill(DeathEvent e) {
        if (e.byPlayer() && hasAtt(e.damagerPlayer())) e.damageSource().Special(name());
    }
}
