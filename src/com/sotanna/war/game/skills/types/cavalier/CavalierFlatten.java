package com.sotanna.war.game.skills.types.cavalier;

import com.sotanna.core.Core;
import com.sotanna.core.effect.Particle;
import com.sotanna.core.generate.types.Cylinder;
import com.sotanna.core.util.F;
import com.sotanna.core.util.item.ItemFactory;
import com.sotanna.core.util.streams.HighestBlock;
import com.sotanna.core.util.task.Run;
import com.sotanna.core.util.uU.VectorU;
import com.sotanna.war.game.build.CavalierWarrior;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import java.util.concurrent.TimeUnit;

public class CavalierFlatten extends WarriorSkill<CavalierFlatten, CavalierWarrior> {

    private static final Vector Jump = new Vector(0, 1.4, 0);

    public CavalierFlatten() {
        super("Flatten", SkillType.Secondary, "Leap into the air and smash the ground", "stunning nearby enemies");
    }

    @Override
    public Long time() {
        return 12000L;
    }

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if (!e.hasItem() || !ItemFactory.isAxe(e.getItem()) || !use(e.getPlayer())) return;

        int level = level(e.getPlayer());
        if (level == 0) return;

        e.getPlayer().setVelocity(Jump);

        Run.submit(() -> e.getPlayer().setFallDistance(-10)).After(20).Until(() -> e.getPlayer().isOnGround()).Done(() -> {
            Cylinder cyl = new Cylinder(e.getPlayer().getLocation().subtract(0, 1, 0), 1).Radius(radius(level)).Filled(true);

            F.map(cyl.algorithm(), HighestBlock.get()).forEach(block -> {
                Particle.playBlockBreak(block);
                Core.Players.nearby(block.getLocation(), 2).forEach(player -> {
                    if (!player.equals(e.getPlayer())) {

                        damage(player, e.getPlayer(), 4, TimeUnit.SECONDS);
                        Core.Damage.newDamage(e.getPlayer(), player, (1.75 * level));
                        player.setVelocity(VectorU.trace(e.getPlayer().getLocation(), player.getEyeLocation()).multiply(1.8));
                    }
                });
            });

        }).goRepeat(1);
    }

    private int radius(int level) {
        return 1 + (2 * (level <= 2 ? 1 : 2));
    }
}
