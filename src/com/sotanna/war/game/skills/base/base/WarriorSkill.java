package com.sotanna.war.game.skills.base.base;

import com.sotanna.core.event.base.EListener;
import com.sotanna.core.util.C;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.game.WarGame;
import com.sotanna.war.game.build.base.WarriorBuild;

import com.sotanna.war.game.skills.SkillType;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public abstract class WarriorSkill <S extends WarriorSkill, W extends WarriorBuild> extends EListener<S> implements SkillStatus {

    private W warrior;

    private SkillType type;
    private String[] description;

    private HashSet<Player> prepared = new HashSet<>();

    public WarriorSkill(String name, SkillType type, String... description) {
        super(name);
        this.type = type;
        this.description = description;
    }

    public SkillType type() {
        return type;
    }

    public String[] description() {
        return description;
    }

    @Override
    public void onDisable() {
        prepared.clear();
    }

    public W warrior() {
        return warrior;
    }

    public S Warrior(W warrior) {
        this.warrior = warrior;
        return thisClass();
    }

    /**
     * Get a player's skill level for this Skill
     *
     * @param player The Player
     * @return 0 if they don't have this, otherwise the level
     */
    public int level(Player player) {
        return warrior().level(player, this);
    }

    public Build build(Player player) {
        return warrior().build(player);
    }

    public WarGame<?> game() {
        return warrior().game();
    }

    @Override
    public TimeUnit timeUnit() {
        return TimeUnit.MILLISECONDS;
    }

    public int maxLevel() {
        return 4;
    }

    public int initWorth() {
        return 2;
    }

    public int worth() {
        return 1;
    }

    public void prepareSkill(Player player, boolean... nomsg) {
        if (nomsg.length == 0) warrior().message("&bYou Prepared &e" + name()).send(player);
        prepared.add(player);
    }

    public boolean prepared(Player player) {
        return prepared.contains(player);
    }

    public HashSet<Player> prepared() {
        return prepared;
    }

    public void useSkill(Player player, int level) {
        warrior().message(C.Msg + "You Used " + C.Value + name() + " " + C.White + level).send(player);
        prepared.remove(player);
    }

    public void toggleSkill(Player player) {
        warrior().message(C.Msg + "You Toggled " + C.Value + name()).send(player);
    }

    public void wearOffSkill(Player player, boolean... nomsg) {
        if (nomsg.length == 0) warrior().message(C.Value + name() + C.Msg +" wore off").send(player);
        prepared.remove(player);
    }

    public void onInteract(PlayerInteractEvent e) {}

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof WarriorSkill)) return false;
        WarriorSkill<?, ?> skill = ((WarriorSkill) obj);

        return skill.name().equals(name()) && skill.warrior().equals(warrior());
    }

    @Override
    public int hashCode() {
        return name().hashCode();
    }
}
