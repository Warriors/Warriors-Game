package com.sotanna.war.game.skills.base.base;

import com.sotanna.core.cool.StatusUser;
import com.sotanna.core.player.Rank;

import org.bukkit.entity.Player;

import java.util.concurrent.TimeUnit;

public interface SkillStatus extends StatusUser {

    @Override
    default boolean use(Player player) {
        return Rank.bypassing(player) || StatusUser.super.use(player);
    }

    @Override
    default boolean use(Player player, Long time) {
        return Rank.bypassing(player) || StatusUser.super.use(player, time);
    }

    @Override
    default boolean use(Player player, Long time, TimeUnit timeUnit) {
        return Rank.bypassing(player) || StatusUser.super.use(player, time, timeUnit);
    }
}
