package com.sotanna.war.game.skills.base;

import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

public abstract class RequiringSkill<S extends RequiringSkill, W extends WarriorBuild> extends WarriorSkill<S, W> {

    public RequiringSkill(String name, SkillType type, String... description) {
        super(name, type, description);
    }

    public abstract SkillType requires();
}
