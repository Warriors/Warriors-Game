package com.sotanna.war.game.skills.base;

import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.SkillType;

public abstract class PassiveSkill<S extends PassiveSkill, W extends WarriorBuild> extends RequiringSkill<S, W> {

    public PassiveSkill(String name, String... description) {
        super(name, SkillType.Passive, description);
    }

    @Override
    public int maxLevel() {
        return 2;
    }

    @Override
    public int initWorth() {
        return 2;
    }

    @Override
    public int worth() {
        return 1;
    }
}
