package com.sotanna.war.game.skills.base;

import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.SkillType;

public abstract class SpecialtySkill<S extends SpecialtySkill, W extends WarriorBuild> extends RequiringSkill<S, W> {

    public SpecialtySkill(String name, String... description) {
        super(name, SkillType.Specialty, description);
    }

    @Override
    public int maxLevel() {
        return 2;
    }

    @Override
    public int initWorth() {
        return 4;
    }

    @Override
    public int worth() {
        return 1;
    }
}
