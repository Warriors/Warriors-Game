package com.sotanna.war.game.skills.base;

import com.sotanna.core.Core;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.event.custom.ArrowHitBlockEvent;
import com.sotanna.core.util.F;
import com.sotanna.core.util.uU.ActionU;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

public abstract class PreparingBowSkill<S extends PreparingBowSkill, W extends WarriorBuild> extends WarriorSkill<S, W> {

    public PreparingBowSkill(String name, SkillType type, String... description) {
        super(name, type, description);
    }

    @Override
    public void onInteract(PlayerInteractEvent e) {
        if (!canPrepare(e)) return;

        if (prepared(e.getPlayer())) return;

        if (use(e.getPlayer())) prepareSkill(e.getPlayer());
        if (cancelEvent()) e.setCancelled(true);
    }

    @EventHandler
    public void onShoot(EntityShootBowEvent e) {
        if (e.isCancelled() || !(e.getEntity() instanceof Player) || !(e.getProjectile() instanceof Arrow)) return;

        Player shooter = ((Player) e.getEntity());

        int level = level(shooter);
        if (level == 0) return;

        if (!prepared(shooter)) return;
        Arrow arrow = ((Arrow) e.getProjectile());

        if (!shouldShoot(e, shooter, arrow, level)) return;

        useSkill(shooter, level);
        arrow.setMetadata(name(), new FixedMetadataValue(Core.Instance, shooter.getUniqueId().toString()));

        onShoot(e, shooter, arrow, level);
    }

    @EventWatcher
    public void onHit(ArrowHitBlockEvent e) {
        if (!e.arrow().hasMetadata(name())) return;
        Arrow arrow = e.arrow();

        Player shooter = Core.Players.byUUID(F.uuid(arrow.getMetadata(name()).get(0).asString()));
        if (null == shooter) return;

        int level = level(shooter);
        if (level == 0) return;

        onHitBlock(e, shooter, level);
    }

    @EventWatcher
    public void onHitP(DamageEvent e) {
        if (!e.isPvP() || !e.byProjectile() || !(e.projectile() instanceof Arrow) || e.cancelled()) return;

        Arrow arrow = ((Arrow) e.projectile());
        if (!arrow.hasMetadata(name())) return;

        Player shooter = Core.Players.byUUID(F.uuid(arrow.getMetadata(name()).get(0).asString()));
        if (null == shooter) return;

        int level = level(shooter);
        if (level == 0) return;

        onHitPlayer(e, shooter, level);
    }


    public boolean canPrepare(PlayerInteractEvent e) {
        return !e.isCancelled() && ActionU.Left.isType(e.getAction()) && e.hasItem();
    }

    public boolean cancelEvent() {
        return true;
    }

    public boolean shouldShoot(EntityShootBowEvent e, Player shooter, Arrow arrow, int level) {
        return true;
    }

    public abstract void onShoot(EntityShootBowEvent e, Player shooter, Arrow arrow, int level);

    public abstract void onHitBlock(ArrowHitBlockEvent e, Player shooter, int level);

    public abstract void onHitPlayer(DamageEvent e, Player shooter, int level);
}
