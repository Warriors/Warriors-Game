package com.sotanna.war.game.skills.base;

import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.SkillType;

public class GlobalSkill<S extends GlobalSkill, B extends WarriorBuild> extends PassiveSkill<S, B> {

    public GlobalSkill(String name, String... description) {
        super(name, description);
    }

    @Override
    public Long time() {
        return 0L;
    }

    @Override
    public SkillType requires() {
        return null;
    }
}
