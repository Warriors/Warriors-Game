package com.sotanna.war.game.skills;

import org.bukkit.Material;

public enum SkillType {

    Weapon    (Material.DIAMOND_SWORD , 1),
    Ranged    (Material.BOW           , 3),
    Secondary (Material.DIAMOND_AXE   , 5),
    Specialty (Material.IRON_FENCE    , 7),
    Passive   (Material.EMERALD       , -1);

    private Material icon;
    private int slot;

    SkillType(Material icon, int slot) {
        this.icon = icon;
        this.slot = slot;
    }

    public Material icon() {
        return icon;
    }

    public int slot() {
        return slot;
    }

    @Override
    public String toString() {
        return name();
    }
}
