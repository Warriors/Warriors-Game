package com.sotanna.war.game.skills.global;

import com.sotanna.core.damage.Condition;
import com.sotanna.core.damage.event.ConditionEvent;
import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.event.bus.events.EventPriority;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.base.GlobalSkill;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;


public class Suppression<B extends WarriorBuild> extends GlobalSkill<Suppression, B> {

    public Suppression() {
        super("Suppression", "You quickly suppress any ", "effects acted upon you");
    }

    @Override
    public boolean notifying() {
        return false;
    }

    @EventWatcher(priority = EventPriority.Monitor)
    public void onFire(DamageEvent e) {
        if (!e.isPlayer() || e.cancelled() || e.cause() != EntityDamageEvent.DamageCause.FIRE && e.cause() != EntityDamageEvent.DamageCause.FIRE_TICK) return;

        int level = level(e.damagedPlayer());
        if (level == 0) return;

        int ticksLeft = e.damagedPlayer().getFireTicks();
        if (ticksLeft >= 20) Condition.Fire.act(e.damagedPlayer(), ticksLeft - (level * 10));
    }

    @EventWatcher(priority = EventPriority.Monitor)
    public void onCondition(ConditionEvent e) {
        if (!(e.entity() instanceof Player) || e.cancelled()) return;

        int level = level(((Player) e.entity()));
        if (level == 0) return;

        e.Time(e.time() - (level * 15));
        if (level > 1 && e.level() > 2) e.Level(2);
    }
}
