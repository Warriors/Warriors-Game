package com.sotanna.war.game.skills.global;

import com.sotanna.core.damage.event.DamageEvent;
import com.sotanna.core.effect.ESound;
import com.sotanna.core.event.bus.events.EventPriority;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.base.GlobalSkill;

import org.bukkit.Sound;
import org.bukkit.event.entity.EntityDamageEvent;

public class BreakFall<B extends WarriorBuild> extends GlobalSkill<BreakFall, B> {

    private static final ESound Break = new ESound(Sound.HORSE_GALLOP, 5, 0.2);

    public BreakFall() {
        super("BreakFall", "Roll as you land to absorb fall damage");
    }

    @EventWatcher(priority = EventPriority.Last)
    public void onFall(DamageEvent e) {
        if (!e.isPlayer() || e.cause() != EntityDamageEvent.DamageCause.FALL || e.cancelled()) return;

        int level = level(e.damagedPlayer());
        if (level == 0) return;

        if (e.damage() >= 10) Break.sNear(e.damagedPlayer().getLocation(), 15);
        e.Damage(e.damage() - (e.damage() * (0.3 * level)));
    }
}
