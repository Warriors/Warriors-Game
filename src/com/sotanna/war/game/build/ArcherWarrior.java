package com.sotanna.war.game.build;

import com.sotanna.core.util.item.Armor;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.build.base.Item;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.global.BreakFall;
import com.sotanna.war.game.skills.global.Suppression;
import com.sotanna.war.game.skills.types.archer.ranged.ArcherFlame;
import com.sotanna.war.game.skills.types.archer.ranged.ArcherPiercingShot;
import com.sotanna.war.game.skills.types.archer.specialty.ArcherRopedArrow;
import com.sotanna.war.game.skills.types.archer.weapon.ArcherPounce;
import com.sotanna.war.game.skills.types.archer.passive.ArcherRetaliate;
import com.sotanna.war.game.skills.types.archer.passive.ArcherSharpShooter;

import org.bukkit.Material;
import org.bukkit.entity.Player;

public class ArcherWarrior extends WarriorBuild<ArcherWarrior> {

    private final Item Bow = new Item(0, Material.BOW, 0, 1, 4);

    public ArcherWarrior() {
        super("Archer",
              new String[]{
                      "Specializes in Ranged Combat"
              },
                new ArcherRetaliate(), new ArcherPounce(), new ArcherSharpShooter(), new ArcherPiercingShot(), new ArcherRopedArrow(), new ArcherFlame(),
                new BreakFall<>(), new Suppression<>());
    }

    @Override
    public Build defaultBuild() {
        Build build = new Build(thisClass().name());
        build.Skill(byName("Flame"), 1);
        build.Skill(byName("Retaliate"), 1);

        build.Armor(Armor.Position.Helmet, Armor.Type.Leather)
             .Armor(Armor.Position.Chestplate, Armor.Type.Leather)
             .Armor(Armor.Position.Leggings, Armor.Type.Leather)
             .Armor(Armor.Position.Boots, Armor.Type.Leather);

        build.Item(Bow);

        return build;
    }

    @Override
    public ArcherWarrior Build(Player player, Build build) {
        return super.Build(player, build);
    }
}
