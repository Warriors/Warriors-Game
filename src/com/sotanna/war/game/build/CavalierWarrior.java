package com.sotanna.war.game.build;

import com.sotanna.core.util.item.Armor;
import com.sotanna.core.util.item.EItem;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.global.BreakFall;
import com.sotanna.war.game.skills.global.Suppression;
import com.sotanna.war.game.skills.types.cavalier.CavalierCharge;
import com.sotanna.war.game.skills.types.cavalier.CavalierFlatten;
import com.sotanna.war.game.skills.types.cavalier.CavalierHiltJab;
import com.sotanna.war.game.skills.types.cavalier.CavalierRive;
import org.bukkit.Material;

public class CavalierWarrior extends WarriorBuild<CavalierWarrior> {

    private final EItem WarHammer = new EItem(Material.GOLD_AXE).Name("&7&lWar Hammer").Unbreakable(true);
    private final EItem Slicer = new EItem(Material.GOLD_SWORD).Name("&7&lSlicer").Unbreakable(true);

    public CavalierWarrior() {
        super("Cavalier",
              new String[]{
                      "The Heart of a Knight",
                      "Skills of a savage"
              },
                new CavalierCharge(), new CavalierRive(), new CavalierHiltJab(), new CavalierFlatten(),
                new BreakFall<>(), new Suppression<>());
    }

    @Override
    public Build defaultBuild() {
        Build build = new Build(thisClass().name());
        build.Skill(byName("Charge"), 2);

        build.Armor(Armor.Position.Helmet, Armor.Type.Iron)
             .Armor(Armor.Position.Chestplate, Armor.Type.Iron)
             .Armor(Armor.Position.Leggings, Armor.Type.Iron)
             .Armor(Armor.Position.Boots, Armor.Type.Iron);

        return build;
    }

    public EItem WarHammer() {
        return WarHammer;
    }

    public EItem Slicer() {
        return Slicer;
    }
}
