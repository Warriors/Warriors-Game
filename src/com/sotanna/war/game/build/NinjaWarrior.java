package com.sotanna.war.game.build;

import com.sotanna.core.util.item.Armor;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.game.build.base.WarriorBuild;

public class NinjaWarrior extends WarriorBuild<NinjaWarrior> {

    public NinjaWarrior() {
        super("Ninja",
                new String[]{
                        "Extremely agile and Mysterious"
                }
                );
    }

    @Override
    public Build defaultBuild() {
        Build build = new Build(thisClass().name());

        build.Armor(Armor.Position.Helmet, Armor.Type.Leather)
                .Armor(Armor.Position.Chestplate, Armor.Type.Leather)
                .Armor(Armor.Position.Leggings, Armor.Type.Leather)
                .Armor(Armor.Position.Boots, Armor.Type.Leather);

        return build;
    }
}
