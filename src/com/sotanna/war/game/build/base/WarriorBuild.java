package com.sotanna.war.game.build.base;

import com.sotanna.core.event.base.EListener;
import com.sotanna.core.util.F;
import com.sotanna.war.Warriors;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.game.WarGame;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.base.WarriorSkill;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.*;
import java.util.stream.Collectors;

public abstract class WarriorBuild<W extends WarriorBuild> extends EListener<W> {

    private final Map<Player, Build> Builds = new HashMap<>();

    private String[] description;
    private Map<SkillType, LinkedHashMap<String, WarriorSkill<?, W>>> SkillsMap = new HashMap<>();

    @SafeVarargs
    public WarriorBuild(String name, String[] description, WarriorSkill<?, W>... skills) {
        super(name);
        this.description = description;

        F.stream(skills).forEach(warriorSkill -> {
            LinkedHashMap<String, WarriorSkill<?, W>> treeMap = SkillsMap.computeIfAbsent(warriorSkill.type(), skillType -> new LinkedHashMap<>());
            treeMap.put(warriorSkill.name(), warriorSkill.Warrior(thisClass()));
        });
    }

    public String[] description() {
        return description;
    }

    public List<Player> users() {
        return Builds.keySet().stream().collect(Collectors.toList());
    }

    public List<WarriorSkill<?, W>> Skills(SkillType skillType) {
        LinkedHashMap<String, WarriorSkill<?, W>> map = SkillsMap.getOrDefault(skillType, new LinkedHashMap<>());
        return F.collect(F.stream(map.values()));
    }

    public WarriorSkill<?, ?> byName(String name) {
        LinkedHashMap<String, WarriorSkill<?, W>> first = F.stream(SkillsMap.values()).filter(map -> map.containsKey(name)).findFirst().orElse(null);
        return first == null ? null : first.get(name);
    }

    public W Build(Player player, Build build) {
        if (build == null) Builds.remove(player);
        else Builds.put(player, build);
        return thisClass();
    }

    public boolean using(Player player) {
        return Builds.get(player) != null;
    }

    public Build build(Player player) {
        return Builds.get(player);
    }

    public int level(Player player, WarriorSkill skill) {
        Build wBuild = Builds.get(player);
        if (wBuild == null) return 0;

        Integer level = wBuild.level(skill.name());
        return level == null ? 0 : level;
    }

    public WarGame<?> game() {
        return Warriors.CurrentGame;
    }

    @Override
    public void onEnable() {
        SkillsMap.values().forEach(map -> map.values().forEach(EListener::enable));
    }

    @Override
    public void onDisable() {
        SkillsMap.values().forEach(map -> map.values().forEach(EListener::disable));
        SkillsMap.clear();

        Builds.clear();
    }

    public abstract Build defaultBuild();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInteract(PlayerInteractEvent e) {
        if (!using(e.getPlayer())) return;
        e.setCancelled(false);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInteractFin(PlayerInteractEvent e) {
        SkillsMap.values().forEach(map -> map.values().forEach(skill -> {
            if (level(e.getPlayer(), skill) > 0) skill.onInteract(e);
        }));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof WarriorBuild)) return false;

        WarriorBuild<?> build = ((WarriorBuild) obj);
        return build.name().equals(name()) && Arrays.equals(build.description(), description());
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(description);
        result = 31 * result + SkillsMap.hashCode();
        return result;
    }
}
