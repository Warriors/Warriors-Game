package com.sotanna.war.game.build;

import com.sotanna.core.util.item.Armor;
import com.sotanna.core.util.item.EItem;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.global.BreakFall;
import com.sotanna.war.game.skills.global.Suppression;
import com.sotanna.war.game.skills.types.savant.SavantFlog;
import com.sotanna.war.game.skills.types.savant.SavantElude;
import com.sotanna.war.game.skills.types.savant.SavantGhost;
import com.sotanna.war.game.skills.types.savant.SavantMassacre;
import org.bukkit.Material;

public class SavantWarrior extends WarriorBuild<SavantWarrior> {

    private final EItem HolyBlade = new EItem(Material.DIAMOND_SWORD).Name("&f&lHoly Blade").Unbreakable(true);
    private final EItem MightyHammer = new EItem(Material.DIAMOND_AXE).Name("&f&lMighty Hammer").Unbreakable(true);

    public SavantWarrior() {
        super("Savant",
              new String[]{
                      "Gentle in Nature",
                      "But extremely powerful when provoked"
              },
                new SavantElude(), new SavantMassacre(), new SavantFlog(), new SavantGhost(),
                new BreakFall<>(), new Suppression<>());
    }

    public EItem HolyBlade() {
        return HolyBlade;
    }

    public EItem MightyHammer() {
        return MightyHammer;
    }

    @Override
    public Build defaultBuild() {
        Build build = new Build(thisClass().name());
        build.Skill(byName("Elude"), 1);

        build.Armor(Armor.Position.Helmet, Armor.Type.Leather)
             .Armor(Armor.Position.Chestplate, Armor.Type.Leather)
             .Armor(Armor.Position.Leggings, Armor.Type.Leather)
             .Armor(Armor.Position.Boots, Armor.Type.Leather);

        return build;
    }
}
