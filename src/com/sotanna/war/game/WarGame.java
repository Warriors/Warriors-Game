package com.sotanna.war.game;

import com.sotanna.core.Core;
import com.sotanna.core.damage.event.DeathEvent;
import com.sotanna.core.event.base.EListener;
import com.sotanna.core.event.bus.events.EventWatcher;
import com.sotanna.core.event.custom.AccountUpdateEvent;
import com.sotanna.core.scoreboard.Board;
import com.sotanna.core.scoreboard.BoardBuilder;
import com.sotanna.core.scoreboard.BoardListener;
import com.sotanna.core.util.task.Run;
import com.sotanna.war.War;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.build.base.StoredBuild;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.HashMap;
import java.util.Map;

import static com.sotanna.core.event.bus.events.EventPriority.Monitor;
import static org.bukkit.event.EventPriority.MONITOR;

public abstract class WarGame <W extends WarGame> extends EListener<W> {

    private String name;
    private WarType type;

    private final Map<Player, Board> Boards = new HashMap<>();
    private final Map<Player, Integer> Scores = new HashMap<>();

    private boolean looseStreak = true;

    public WarGame(String name, WarType type) {
        super(name);
        this.name = name;
        this.type = type;
    }

    public String name() {
        return name;
    }

    public WarType type() {
        return type;
    }

    public boolean looseStreak() {
        return looseStreak;
    }

    public WarGame LooseStreak(boolean looseStreak) {
        this.looseStreak = looseStreak;
        return this;
    }

    public abstract BoardBuilder boardBuilder(Player player);

    public BoardListener boardListener(Player player) {
        return new BoardListener() {
            @EventWatcher(priority = Monitor)
            public void onKillDeath(DeathEvent e) {
                if (e.cancelled()) return;
                board().update();
            }

            @EventWatcher
            public void onRank(AccountUpdateEvent e) {
                board().update();
                if (e.account().localOnline()) board().OTeam(e.account().player(), e.account().rank().boardName());
            }
        };
    }

    public W join(Player player) {
        Scores.put(player, 0);
        board(player);

        updateBoard();
        return thisClass();
    }

    public W quit(Player player) {
        Scores.remove(player);

        if (Core.Enabled) updateBoard();
        Board board = Boards.remove(player);
        if (board != null) board.destroy();

        return thisClass();
    }

    public int score(Player player) {
        return Scores.containsKey(player) ? Scores.get(player) : 0;
    }

    public void Reset(Player player) {
        Scores.put(player, 0);
    }

    public void ScoreMod(Player player, int amount) {
        Integer current = Scores.get(player);
        Scores.put(player, current == null ? 0 : current + amount);
    }

    public Board board(Player player) {
        Board board = new Board(player) {
            @Override
            public String title() {
                return "   &3&lWarriors " + type().name() + "   ";
            }

            @Override
            public BoardBuilder boardBuilder() {
                return WarGame.this.boardBuilder(player);
            }
        }.BoardListener(boardListener(player)).update();

        Board old = Boards.remove(player);
        if (old != null) old.destroy();

        Boards.put(player, board);
        return board;
    }

    public Map<Player, Board> Boards() {
        return Boards;
    }

    @EventWatcher
    public void onKill(DeathEvent e) {
        if (!e.isPvP() || e.cancelled()) return;
        if (looseStreak()) Reset(e.damagedPlayer());
        ScoreMod(e.damagerPlayer(), 1);
    }

    @EventHandler(priority = MONITOR)
    public void onRespawn(PlayerRespawnEvent e) {
        StoredBuild storedBuild = War.Warriors.build(e.getPlayer());

        for (Build build : storedBuild.builds()) {
            if (build.using(e.getPlayer())) {
                build.enable(e.getPlayer());
                break;
            }
        }
    }

    private void updateBoard() {
        Run.submit(() -> Boards.values().forEach(Board::update)).goLater(10);
    }
}
