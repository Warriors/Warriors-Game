package com.sotanna.war.game;

public class WarType {

    public static final WarType TDM    = new WarType("TDM"    , "Fight to the death");
    public static final WarType KitPvP = new WarType("KitPvP" , "Who can get the highest streak!?!?!");

    private String name;
    private String[] description;

    public WarType(String name, String... description) {
        this.name = name;
        this.description = description;
    }

    public String name() {
        return name;
    }

    public String[] description() {
        return description;
    }
}
