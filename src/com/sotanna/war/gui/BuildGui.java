package com.sotanna.war.gui;

import com.sotanna.core.gui.EGui;
import com.sotanna.core.gui.base.ClickHandler;
import com.sotanna.core.gui.base.Clicker;
import com.sotanna.core.util.item.EItem;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.build.base.StoredBuild;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class BuildGui extends EGui {

    private final StoredBuild storedBuild;
    private WeakReference<Player> player;

    public BuildGui(Player player, StoredBuild storedBuild) {
        super("BuildGui:"+player.getName(), "&3&lBuilds", 27);
        this.storedBuild = storedBuild;
        this.player = new WeakReference<>(player);
    }

    @Override
    public void onBuild() {
        int slot = 9;
        while (slot++ < 17 && storedBuild.builds().size() > slot - 10) {
            Build build = storedBuild.builds().get(slot - 10);
            WBuildGui wBuildGui = new WBuildGui(this, player.get(), build);
            boolean enabled = build.using(player.get());

            add(new EItem(Material.BOOK_AND_QUILL).Glow(enabled)
                                                  .Name("&f&l"+build.name() + " Build")
                                                  .Lore(lore(build)).get(), new ClickHandler(slot) {
                @Override
                public void call(Clicker data) {
                    if (storedBuild.locked()) playFail(data.player());
                    else {
                        if (enabled && data.clickType() == ClickType.LEFT) return;

                        storedBuild.builds().forEach(b -> {
                            if (!b.equals(build)) b.disable(data.player());
                        });
                        build.enable(data.player());

                        if (data.clickType() == ClickType.RIGHT) {
                            playInteract(data.player());
                            wBuildGui.open(data.player());
                        }
                        else playSuccess(data.player());

                        build();
                    }
                }
            });
        }
    }

    @Override
    public void genericClick(Clicker clicker) {}

    @Override
    public void onNoViewers() {}

    private List<String> lore(Build build) {
        List<String> lore = new ArrayList<>();
        boolean enabled = build.using(player.get());

        lore.add(" ");
        lore.add("&8&l&m------------------------");
        lore.add(" ");
        for (String line : build.warrior().description()) lore.add(" &f&l" + line);
        lore.add(" ");
        lore.add("&8&l&m------------------------");
        lore.add(" ");

        lore.add(!enabled ? "&7(Left Click to Enable)" : "&a&lCurrently Chosen");
        lore.add("&c(Right Click to Modify)");

        return lore;
    }
}
