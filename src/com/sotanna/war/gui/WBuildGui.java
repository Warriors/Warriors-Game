package com.sotanna.war.gui;

import com.sotanna.core.gui.EGui;
import com.sotanna.core.gui.PEGui;
import com.sotanna.core.gui.base.ClickHandler;
import com.sotanna.core.gui.base.Clicker;
import com.sotanna.core.util.C;
import com.sotanna.core.util.F;
import com.sotanna.core.util.item.Armor;
import com.sotanna.core.util.item.EItem;
import com.sotanna.core.util.task.Run;
import com.sotanna.war.War;
import com.sotanna.war.build.base.Build;
import com.sotanna.war.build.base.Item;
import com.sotanna.war.game.build.base.WarriorBuild;
import com.sotanna.war.game.skills.SkillType;
import com.sotanna.war.game.skills.base.RequiringSkill;
import com.sotanna.war.game.skills.base.base.WarriorSkill;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class WBuildGui extends EGui {

    private static final Item
            IronSword  = new Item(-1, Material.IRON_SWORD, 0, 1, 2),
            DiaSword   = new Item(-1, Material.DIAMOND_SWORD, 0, 1, 4),

            IronAxe = new Item(-1, Material.IRON_AXE, 0, 1, 2),
            DiaAxe = new Item(-1, Material.DIAMOND_AXE, 0, 1, 4),

            Bow        = new Item(-1, Material.BOW, 0, 1, 4),
            Arrows     = new Item(-1, Material.ARROW, 0, 30, 3);

    private static final int[] PASSIVE_SLOTS = new int[]{19, 20, 21, 22, 23, 24, 25};
    private static final int[] ARMOR_SLOTS = new int[]{9, 11, 15, 17};

    private BuildGui master;
    private Build build;
    private WarriorBuild<?> warrior;

    private Player player;

    public WBuildGui(BuildGui master, Player player, Build build) {
        super(master.name() + ":" + build.name(), "&3&l" + build.name() + " Build", 54);
        this.master = master;
        this.build = build;
        this.warrior = War.Warriors.byName(wBuild().name());
        this.player = player;

        PlayerView(new PEGui(thisClass(), player) {
            @Override
            public void onBuild() {
                player.getInventory().clear();

                List<? extends WarriorSkill<?, ?>> skills = warrior.Skills(SkillType.Passive);
                final int[] on = {0};
                for (int slot : PASSIVE_SLOTS) {
                    WarriorSkill<?, ?> skill = skills.size() > on[0] ? skills.get(on[0]++) : null;
                    add(forSkill(skill).get(), skillHandler(skill, slot));
                }
                on[0] = 0;

                Map<Armor.Position, Armor.Type> armor = wBuild().ArmorMap();
                armor.forEach(((position, type) -> add(forArmor(position, type).get(), armorHandler(position, type, ARMOR_SLOTS[on[0]++]))));

                LinkedHashMap<Integer, Item> items = wBuild().ItemMap();

                add(IronSword.get().get() , items.values().contains(IronSword) ? ClickHandler.DoNothing(27) : itemHandler(IronSword , 27));
                add(DiaSword.get().get()  , items.values().contains(DiaSword) ? ClickHandler.DoNothing(28) : itemHandler(DiaSword  , 28));
                add(IronAxe.get().get()   , items.values().contains(IronAxe) ? ClickHandler.DoNothing(29) : itemHandler(IronAxe   , 29));
                add(DiaAxe.get().get()    , items.values().contains(DiaAxe) ? ClickHandler.DoNothing(30) : itemHandler(DiaAxe    , 30));
                add(Bow.get().get()       , items.values().contains(Bow) ? ClickHandler.DoNothing(32) : itemHandler(Bow       , 32));
                add(Arrows.get().get()    , itemHandler(Arrows    , 33));

                items.forEach((slot, item) -> add(item.get().get(), ownedItemHandler(item)));
            }

            @Override
            public void genericClick(Clicker clicker) {}

            @Override
            public void onNoViewers() {}
        });
    }

    public BuildGui master() {
        return master;
    }

    public Build wBuild() {
        return build;
    }

    @Override
    public void onBuild() {

        if (warrior == null) for (int i = 0; i < size(); i++) add(new EItem(Material.BARRIER).Name("&4&lWarrior not found").get(), ClickHandler.DoNothing(nextSlot()));
        else {

            for (SkillType type : SkillType.values()) {
                if (type == SkillType.Passive) break;

                add(new EItem(type.icon()).Name("&f&l" + type.name()).Flag(ItemFlag.HIDE_ATTRIBUTES).get(), ClickHandler.Call(type.slot(), data -> playInteract(data.player())));
                Iterator<? extends WarriorSkill<?, ?>> skills = warrior.Skills(type).iterator();

                for (int slot : slots(type.slot())) {
                    WarriorSkill<?, ?> skill = skills.hasNext() ? skills.next() : null;
                    add(forSkill(skill).get(), skillHandler(skill, slot));
                }
            }

            add(new EItem(Material.NETHER_STAR).Name("&d&lActive Skill Tokens").Amount(wBuild().refreshBal().skillTokens()).get(), ClickHandler.Call(47, clicker -> playFail(clicker.player())));
            add(new EItem(Material.STICK).Name("&d&lItem Tokens").Amount(wBuild().refreshBal().itemTokens()).get(), ClickHandler.Call(49, clicker -> playFail(clicker.player())));
            add(new EItem(Material.EMERALD).Name("&d&lPassive Skill Tokens").Amount(wBuild().refreshBal().passiveTokens()).get(), ClickHandler.Call(51, clicker -> playFail(clicker.player())));
        }
    }

    @Override
    public void genericClick(Clicker clicker) {}

    @Override
    public void onNoViewers() {
        if (player != null) {
            player.getInventory().clear();
            player.getInventory().setArmorContents(new ItemStack[4]);
            wBuild().enable(player);
        }
        Run.submit(() -> {
            master().open(player);
            playSuccess(player);
            player = null;
        }).goLater(1);
    }

    private int[] slots(int start) {
        int[] slots = new int[3];
        slots[0] = start = start + 18;
        for (int i = 1; i < 3; i++) slots[i] = start + (9 * i);
        return slots;
    }

    private EItem forSkill(WarriorSkill<?, ?> skill) {
        int level = skill == null ? 0 : skill.level(player);
        boolean enabled = level > 0;
        EItem item = new EItem(Material.INK_SACK, skill == null || !enabled ? 8 : 10);
        item.Name(skill != null ? "&b&l" + skill.name() : "&7Coming Soon...");
        item.Lore(lore(skill, level, enabled));
        item.Glow(enabled);

        return item;
    }

    private EItem forArmor(Armor.Position position, Armor.Type type) {
        EItem item = new EItem(position.type(type)).Name("&a&l" + F.name(position.type(type).name()));
        item.Lore(lore(type));

        return item;
    }

    private List<String> lore(WarriorSkill<?, ?> skill, int level, boolean enabled) {
        List<String> lore = new ArrayList<>();
        if (skill == null) return lore;

        boolean maxed = level == skill.maxLevel();

        lore.add(" ");
        lore.add(C.C("Lore") + "Level " + C.Value + level + C.Gray + "/" + C.Value + skill.maxLevel());
        lore.add(" ");
        lore.add(C.C("LSep") + " --======================--");
        lore.add(" ");
        for (String line : skill.description()) lore.add(C.C("Name") + " " + line);
        lore.add(" ");
        lore.add(C.C("LSep") + " --======================--");
        lore.add(" ");

        SkillType required = skill instanceof RequiringSkill ? ((RequiringSkill) skill).requires() : null;

        if (required != null) {
            if (wBuild().skills(required).isEmpty()) lore.add(C.C("Bad") + "  Requires a " + required.name() + " Skill");
            else lore.add(C.C("Good") + "  Requirements met!");
        }

        lore.add(" ");
        lore.add(!enabled ? C.Gray + "(Click to Enabled)" : !maxed ? C.Msg + "(Left Click to Level up | Right Click to Level down)" : C.Msg + "(Skill level maxed out)");
        if (enabled) lore.add(C.Red + "(Middle Click to disable)");

        return lore;
    }

    private List<String> lore(Armor.Type type) {
        List<String> lore = new ArrayList<>();
        lore.add(type.last() == null ? C.Msg + "(Left Click to Level up)" : C.Msg + (type.next() != null ? "(Left Click to Level up | Right Click to Level down)" : "(Armor maxed out)"));
        lore.add(C.Red + "(Middle Click to reset)");

        return lore;
    }

    private ClickHandler skillHandler(WarriorSkill<?, ?> skill, int slot) {
        return new ClickHandler(slot) {
            @Override
            public void call(Clicker data) {
                if (skill == null) playFail(data.player());
                else {
                    int level = skill.level(player);
                    boolean enabled = level > 0;

                    switch (data.clickType()) {
                        case LEFT:
                            if (enabled) {
                                if (!wBuild().afford(skill, true)) playFail(data.player());
                                else levelUp(skill, level);
                            } else {
                                if (wBuild().skills(skill.type()).size() > 0  && skill.type() != SkillType.Passive) enable(skill);
                                else {
                                    if (!wBuild().afford(skill, false)) playFail(data.player());
                                    else enable(skill);
                                }
                            }
                            break;
                        case RIGHT:
                            if (!enabled) playFail(data.player());
                            else {
                                if (level == 1) disable(skill);
                                else levelDown(skill, level);
                            }
                            break;
                        case MIDDLE:
                            if (!enabled) playFail(data.player());
                            else disable(skill);
                            break;
                        default:
                            playInteract(data.player());
                    }
                }
            }
        };
    }

    private ClickHandler armorHandler(Armor.Position position, Armor.Type type, int slot) {
        return new ClickHandler(slot) {
            @Override
            public void call(Clicker data) {
                switch (data.clickType()) {
                    case LEFT:
                        Armor.Type next = type.next();
                        if (next == null) playFail(data.player());
                        else {
                            if (!wBuild().afford(position, next)) playFail(data.player());
                            else {
                                wBuild().Armor(position, next);
                                wBuild().refreshBal();
                                playSuccess(data.player());
                                build();
                            }
                        }
                        break;
                    case RIGHT:
                        Armor.Type last = type.last();
                        if (last == null) playFail(data.player());
                        else {
                            wBuild().Armor(position, last);
                            wBuild().refreshBal();
                            playSuccess(data.player());
                            build();
                        }
                        break;
                    case MIDDLE:
                        if (type == Armor.Type.Leather) playFail(data.player());
                        else {
                            wBuild().Armor(position, Armor.Type.Leather);
                            wBuild().refreshBal();
                            playSuccess(data.player());
                            build();
                        }
                        break;
                    default:
                        playInteract(data.player());
                }
            }
        };
    }

    private ClickHandler itemHandler(Item item, int slot) {
        return new ClickHandler(slot) {
            @Override
            public void call(Clicker data) {
                if (!wBuild().afford(item)) playFail(player);
                else {
                    Integer firstSlot = null;
                    for (int i = 0; i < 9; i++) {
                        if (player.getInventory().getItem(i) == null) {
                            firstSlot = i;
                            break;
                        }
                    }

                    if (firstSlot == null) playFail(player);
                    else {
                        wBuild().Item(Item.change(item, firstSlot));
                        wBuild().refreshBal();
                        playSuccess(player);
                        build();
                    }
                }
            }
        };
    }

    private ClickHandler ownedItemHandler(Item item) {
        return new ClickHandler(item.slot()) {
            @Override
            public void call(Clicker data) {
                if (data.clickType() != ClickType.RIGHT) playInteract(player);
                else {
                    wBuild().Item(new Item(item.slot(), null, 0, 0, 0));
                    wBuild().refreshBal();
                    playSuccess(player);
                    build();
                }
            }
        };
    }

    private void enable(WarriorSkill<?, ?> skill) {
        if (skill.type() != SkillType.Passive) warrior.Skills(skill.type()).forEach(warriorSkill -> wBuild().Skill(warriorSkill, -1));

        SkillType required = skill instanceof RequiringSkill ? ((RequiringSkill) skill).requires() : null;

        if (required != null && wBuild().skills(required).isEmpty()) {
            playFail(player);
            return;
        }

        wBuild().Skill(skill, 1);
        wBuild().refreshBal();
        playSuccess(player);
        build();
    }

    private void disable(WarriorSkill<?, ?> skill) {
        wBuild().skills(SkillType.Passive, SkillType.Specialty).forEach(requiring -> {
            SkillType required = requiring instanceof RequiringSkill ? ((RequiringSkill) requiring).requires() : null;
            if (required != null && required == skill.type()) wBuild().Skill(requiring, -1);
        });

        wBuild().Skill(skill, -1);
        wBuild().refreshBal();
        playSuccess(player);
        build();
    }

    private void levelDown(WarriorSkill<?, ?> skill, int level) {
        wBuild().Skill(skill, level - 1);
        playSuccess(player);
        wBuild().refreshBal();
        build();
    }

    private void levelUp(WarriorSkill<?, ?> skill, int level) {
        if (level < skill.maxLevel()) {
            wBuild().Skill(skill, level + 1);
            wBuild().refreshBal();
            playSuccess(player);
            build();
        }
    }
}
