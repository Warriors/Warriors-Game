package com.sotanna.war.json;

import com.sotanna.core.Core;
import com.sotanna.core.database.JsonStore;
import com.sotanna.war.War;

import org.bukkit.entity.Player;

import java.io.File;
import java.util.UUID;

public class ScoreStore extends JsonStore<ScoreStore, UUID, Score> {

    public ScoreStore() {
        super(War.Instance, Score.class, new File(Core.ServerFolder,  "Scores"));
    }

    @Override
    public void onEnable() {
        onlineOnJoin();
    }

    @Override
    public void onDisable() {
        onlineOnQuit();
    }

    @Override
    public Score create(UUID id, Object... args) {
        return new Score(id);
    }

    @Override
    public void onJoin(Player player) {
        info("On Join " + player.getName());
        loadOrCreate(player.getUniqueId());
    }

    @Override
    public void onQuit(Player player) {
        Score score = get(player.getUniqueId());
        store(score, false, true);
    }
}
