package com.sotanna.war.json;

import com.sotanna.core.database.json.JFile;

import java.util.UUID;

public class Score extends JFile<Score, UUID> {

    private Integer Kills = 0, Deaths = 0;

    public Score(UUID id) {
        super(id);
    }

    public Integer Kills() {
        return Kills;
    }

    public Score Kills(Integer kills) {
        Kills = kills;
        return this;
    }

    public Integer Deaths() {
        return Deaths;
    }

    public Score Deaths(Integer deaths) {
        Deaths = deaths;
        return this;
    }

    @Override
    public Class<? extends Score> type() {
        return Score.class;
    }

    @Override
    public int compareTo(Score o) {
        return Kills.compareTo(o.Kills) + Deaths.compareTo(o.Deaths);
    }
}
